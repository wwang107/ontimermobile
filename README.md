# Website
See the website for a fun and proper introduction of the app!
https://wwang107.github.io/OnTimer-en/

# Demo of the App
![OnTimer-demo](./demo-images/demo.gif)

# To run the app
1. Install ```Android Studio``` and ```react-native-cli``` by following the guid on [React Native guide](https://facebook.github.io/react-native/docs/getting-started)
2. excute ```react-native run-android```

There you go :D