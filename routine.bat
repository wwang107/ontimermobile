npm install

unamestr=`uname`
if [[ "$unamestr" == 'Darwin' ]]; then
  sed -i '' 's/compileOnly/provided/g; s/implementation/compile/g' node_modules/react-native-maps/lib/android/build.gradle
else
  sed -i 's/compileOnly/provided/g; s/implementation/compile/g' node_modules/react-native-maps/lib/android/build.gradle
fi
