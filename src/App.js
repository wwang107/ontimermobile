import React from 'react';
import { Text, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { createBottomTabNavigator, NavigationActions, createStackNavigator} from 'react-navigation';
import Home from './components/HomeScreen';
import EventForm from './components/EventForm';
import Account from './components/AccountScreen';
import Notification from './components/NotificationScreen';
import EventScreen from './components/EventScreen'
import LoginScreen from './components/LoginScreen';
import EditProfile from './components/AccountScreenSubpage/EditProfile';
import CreateUser from './components/AccountScreenSubpage/CreateUser';
import EditPage from './components/AccountScreenSubpage/EditPage';
import SignInPage from './components/SignInSystem/SignInPage';

// testNotification
import testScreen from './components/test';


import InfoForm from './components/EventFormScreenSubpage/InfoForm';
import CreateEventScreen from './components/EventFormScreenSubpage/createEventScreen';
import JoinEventScreen from './components/EventFormScreenSubpage/JoinEventScreen';
import AboutPage from './components/AccountScreenSubpage/AboutPage';
import TermsOfServicePage from './components/AccountScreenSubpage/TermsOfServicePage';
import PrivatePolicyPage from './components/AccountScreenSubpage/PrivatePolicyPage';
import PaymentPage from './components/AccountScreenSubpage/PaymentPage';
import EditNotification from './components/AccountScreenSubpage/EditNotification';
import Splash from './components/Splash';
import { YellowBox } from 'react-native';

//reducer
import {event as eventReducer} from './states/event-reducers'
import {userInfo} from "./states/user-reducers.js";
import {infoForm} from './states/infoForm-reducers.js';
import {flagControler} from "./states/flag-reducers.js";
import {cashflowItem, invitationsItem, unconfirmedItem} from './states/notifications-reducers';

import {createStore, combineReducers, compose, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import loggerMiddleware from 'redux-logger';
import {Provider, connect} from 'react-redux';




import {
  createReduxBoundAddListener,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader', 'Warning: Failed prop type: Invalid prop `easing`']);





const HomeStack = createStackNavigator({
  Home: Home,
  Detail:EventScreen,
  CreateEventScreen: CreateEventScreen,
  JoinEventScreen: JoinEventScreen,
});

// const AccountSubStack = createStackNavigator({
//   EditProfileNav: EditProfile
// });

const AccountStack = createStackNavigator({
  Account: Account,
  EditProfileNav: EditProfile,
  CreateUserNav: CreateUser,
  EditPageNav: EditPage,
  SignInPageNav: SignInPage,
  AboutPageNav: AboutPage,
  TermsOfServicePageNav: TermsOfServicePage,
  PrivatePolicyPageNav: PrivatePolicyPage,
  PaymentPageNav: PaymentPage,
  EditNotificationNav: EditNotification,  
});


const EventStack = createStackNavigator({
  EventForm: EventForm,
  CreateEventScreen: CreateEventScreen,
  JoinEventScreen: JoinEventScreen,
});


const AppNavigatorHome = createBottomTabNavigator({
  Home: HomeStack,
  // 'Add Event': EventStack,
  Notification: Notification,
  Account: AccountStack,
},
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home'){
          iconName = `ios-menu${focused ? '' : '-outline'}`;
        }else if (routeName === 'Add Event'){
          iconName = `ios-add${focused ? '' : '-outline'}`;
        } else if (routeName === 'Notification') {
          iconName = `ios-notifications${focused ? '' : '-outline'}`;
        } else if (routeName === 'Account'){
          iconName = `ios-contact${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons https://oblador.github.io/react-native-vector-icons/
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'gray',
    },
    tabBarPosition: "bottom",
  }
);

const AppNavigator =  createStackNavigator({
    Splash: {screen: Splash},
    Login: {screen: LoginScreen},
    HomeNav: {screen: AppNavigatorHome},
    // EditProfileNav: {screen: EditProfile}
}, {
    headerMode: 'none'
});

//navreducer
const nav = createNavigationReducer(AppNavigator);


const navMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const addListener = createReduxBoundAddListener("root");

class AppWithStyleAndNavigator extends React.Component {

    render() {

      return (
            <AppNavigator navigation={{
                dispatch: this.props.dispatch,
                state: this.props.nav,
                addListener,
            }}/>
        );
    }
}

const AppWithNavState = connect(state => ({
    nav: state.nav
}))(AppWithStyleAndNavigator);


const store = createStore(combineReducers({
    nav,
    eventReducer, userInfo,
    flagControler,infoForm,
    cashflowItem,
    invitationsItem,
    unconfirmedItem,
    eventReducer,
    flagControler,
    infoForm,
}), compose(applyMiddleware(thunkMiddleware, loggerMiddleware, navMiddleware)));

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppWithNavState />
      </Provider>
    );
  }
}
