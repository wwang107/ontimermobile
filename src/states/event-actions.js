import {listEvents as getEventFromApi,
        signInEvent as signInEventFromApi } from '../api/events'

function loadEvent(){
    return {
        type:'@HOME/LOADING_EVENT'
    };
}

function loadDeviceLocation(){
    return {
        type: '@HOME/LOADING_DEVICE_LOCATION'
    };
}


function setEvent(events){
    return {
        type:'@HOME/SET_EVENT',
        events:events
    }
}


function endLoadingEvent(){
    return {
        type:'@HOME/END_LOADING_EVENT'
    };
}

function endLoadingDeviceLocation() {
    return {
        type: '@HOME/END_LOADING_DEVICE_LOCATION'
    };
}

function setLocation(lat,long){
    return {
        type:'@HOME/SET_LOCATION',
        lat:lat,
        long:long
    }
}

export function getEvent(userId){
    return (dispatch, getState) => {
        dispatch(loadEvent());
        let newEvents = [];
        // TODO: remove the hack method "SET_EVENT"
        // The commented command would be the right approach, and yet the state of the reducer does not alter properly
        // Thus, I use a hack "SET_EVENT".
        return getEventFromApi(userId).then(events =>{
            // dispatch(endGetEvent(events));
            newEvents = events
        }).catch(err=>{
            console.log('Error geting events: ', err);
            // dispatch(resetEvent());
            newEvents = [];
        }).finally(()=>{
            dispatch(setEvent(newEvents));
            dispatch(endLoadingEvent());
        });
    }
}

function signUpEvent(userInfo, deviceLocation, eventLocation, eventTime){
    var currentdate = new Date();
    if (currentdate - eventTime > 30)
        return null; // sign up too early
    
    d = _measure(deviceLocation.lat, deviceLocation.long, eventLocation.lat, eventLocation.long);
    
    if (d <= 50)
        return userInfo;
    else 
        return null;
}

export function getDeviceLoction(options){
    return (dispatch, getState) => {
        dispatch(loadDeviceLocation());

        return _getLocation(options)
                .then(location=>{
                    dispatch(setLocation(location.coords.latitude,location.coords.longitude));
                })
                .catch(err=>{
                    console.log("Error getting device loaction: ", err)
                    setLocation('na','na');
                })
                .finally(()=>{
                    dispatch(endLoadingDeviceLocation());
                });
    }
}

function _getLocation(options) {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
}
