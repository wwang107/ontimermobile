
const initFlagControler = {
    backListenerFlag: false,
    isEdited: false,
    allNotificationFlag: true,
    newInviteNotificationFlag: true,
    upcomingNotificationFlag: true,
};
export function flagControler(state = initFlagControler, action) {
    switch (action.type) {
        case '@FLAG_CONTROLER/BACK_LISTENER_IS_ON':
            return {
                ...state,
                backListenerFlag: true,
            };
        case '@FLAG_CONTROLER/TOGGLE_ALL_NOTIFICATION':
            return {
                ...state,
                allNotificationFlag: !state.allNotificationFlag,
            };
        case '@FLAG_CONTROLER/TOGGLE_NEW_INVITE_NOTIFICATION':
            return {
                ...state,
                newInviteNotificationFlag: !state.newInviteNotificationFlag,
            };
        case '@FLAG_CONTROLER/TOGGLE_UPCOMING_NOTIFICATION':
            return {
                ...state,
                upcomingNotificationFlag: !state.upcomingNotificationFlag,
            };
        default:
            return state;
    }
}
