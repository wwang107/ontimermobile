import {
    getUserInfoLocal as getUserInfoLocalFromApi,
    storeUserInfoLocal as storeUserInfoLocalFromApi,
    calculatePrepareTime as calculatePrepareTimeFromApi,
    getUserInfo as getUserInfoFromApi,
    createUser  as createUserFromApi,
    editUserInfo as editUserInfoFromApi,
    uploadUserPrepareTime as uploadUserPrepareTimeFromApi
} from "../api/userInfos.js";

import {getPaymentList, setPaymentList} from "../api/payment.js"

import {getUserInfo as getInfoFromFbApi} from "../api/fb.js"

export function updateUserName(userName){

    return {
        type: '@USER_INFO/UPDATE_USER_NAME',
        userName
    };
}

export function updateUserPhoneNumber(userPhoneNumber){

    return {
        type: '@USER_INFO/UPDATE_USER_PHONE_NUMBER',
        userPhoneNumber
    };
}

export function updateUserEmail(userEmail){
    return {
        type: '@USER_INFO/UPDATE_USER_EMAIL',
        userEmail
    };
}

export function updateUserGender(userGender){
    return {
        type: '@USER_INFO/UPDATE_USER_GENDER',
        userGender
    };
}

export function updateUserHomeLocation(userHomeLocation){
    return {
        type: '@USER_INFO/UPDATE_USER_HOME_LOCATION',
        userHomeLocation
    };
}

export function updateUserPrepareTime(userPrepareTime){
    console.log(userPrepareTime);
    return {
        type: '@USER_INFO/UPDATE_USER_PREPARE_TIME',
        userPrepareTime
    };
}


// GET user info from server
export function getUserInfo(userId){
  return (dispatch, getState) => {
        dispatch(startFetchUserInfo());
        return getUserInfoFromApi(userId).then(resUserInfo => {
            if(resUserInfo === undefined) throw "return value is undefined";
            dispatch(endFetchUserInfo(resUserInfo[0]));
        }).catch(err => {
            // dispatch(endFetchUserInfo());
            console.log('[Get user info] '+ err);
        });
    };
}

function startFetchUserInfo(){
  return {
      type: '@USER_INFO/START_FETCH_USER_INFO',
  };
}

function endFetchUserInfo(userInfo){
  return {
      type: '@USER_INFO/END_FETCH_USER_INFO',
      userInfo
  };
}

// post EDITED user info to server
export function editUserInfo(userInfo){
  return (dispatch, getState) => {
        dispatch(startEditUserInfo());
        return editUserInfoFromApi(userInfo).then(resUserInfo => {
            if(resUserInfo === undefined) throw "return value is undefined";
            dispatch(endEditUserInfo(resUserInfo));
            dispatch(unsetIsEdited());
        }).catch(err => {
            //dispatch(endEditUserInfo());
            console.log('[Edit to server] '+ err);
        });
    };
}

export function uploadUserPrepareTime(userPrepareTime, userId){
    return (dispatch, getState) => {
        dispatch(startUploadingUserPrepareTime());
        return uploadUserPrepareTimeFromApi(userPrepareTime, userId)
                .then(resUserInfom => {
                    if (resUserInfo === undefined) throw 'return value is undefined after uploading user prepare time'
                    dispatch(endEditUserInfo(resUserInfo))
                    dispatch(unsetIsEdited());
                })
                .catch(err => {
                    console.log('[Edit to server] ' + err);
                }) 
    }
}
export function function_name (argument) {
    // body... 
}
function startEditUserInfo(){
  return {
      type: '@USER_INFO/START_EDIT_USER_INFO',
  };
}

function startUploadingUserPrepareTime(){
    return {
        type: '@USER_INFO/START_UPLOAD_USER_PREPARE_TIME'
    };
}

function endEditUserInfo(userInfo){
  return {
      type: '@USER_INFO/END_EDIT_USER_INFO',
      userInfo
  };
}



// post CREATE user to server
export function createUser(userInfo){
  return (dispatch, getState) => {
        dispatch(startCreateUser());
        return createUserFromApi(userInfo).then(resUserInfo => {
            if(resUserInfo === undefined) throw "return value is undefined";
            dispatch(endCreateUser(resUserInfo));
        }).catch(err => {
            // dispatch(endCreateUser());
            console.log('[Create user] '+ err);
        });
    };
}

function startCreateUser(){
  return {
      type: '@USER_INFO/START_CREATE_USER',
  };
}

function endCreateUser(userInfo){
  return {
      type: '@USER_INFO/END_CREATE_USER',
      userInfo
  };
}

export function setIsEdited(){
    return {
        type: '@USER_INFO/SET_IS_EDITED'
    };
}

export function unsetIsEdited(){
    return {
        type: '@USER_INFO/UNSET_IS_EDITED'
    };
}



//get user info from fb

export function setFbInfo(info){
    return {
        type: 'USER_INFO/SET_FB_INFO',
        info
    }
}

export function setFbUserInfo(){
    return (dispatch, getState) => {
        return getInfoFromFbApi().then((info)=>{
            dispatch(setFbInfo(info));
        }).catch((error)=>{
            throw new Error(error);
        });
    };
}

// set/get payment list to/from local storage
export function setUserPayment(userPaymentList){

    setPaymentList(userPaymentList).catch(err => {
        console.log('[Set user payment fail] '+ err);
    });
    return {
        type: '@USER_INFO/SET_USER_PAYMENT',
        userPaymentList
    };
}

export function getUserPayment(){
    return (dispatch, getState) => {
        dispatch(startGetUserPayment());
        return getPaymentList().then(userPaymentListStr => {
            if(userPaymentList === "") throw "payment list is empty";
            const userPaymentList = JSON.parse(userPaymentListStr);
            dispatch(endGetUserPayment(userPaymentList));
        }).catch(err => {
            console.log('[Get user payment] '+ err);
        });
    };
}

function startGetUserPayment(){
    return {
        type: '@USER_INFO/START_GET_USER_PAYMENT'
    };
}
function endGetUserPayment(userPaymentList){
    return {
        type: '@USER_INFO/END_GET_USER_PAYMENT',
        userPaymentList
    };
}

export function storeUserInfoLocal(userInfo){
    storeUserInfoLocalFromApi(userInfo).catch(err => {
        console.log('[Store user info to local fail] '+ err);
    });
    return {
        type: '@USER_INFO/STORE_USER_INFO_LOCAL'
    };
}

export function getUserInfoLocal(){
    return (dispatch, getState) => {
        dispatch(startGetUserInfoLocal());
        return getUserInfoLocalFromApi().then(userInfo => {
            if(userPaymentList === "") throw "userInfo is empty";
            const userPaymentList = JSON.parse(userInfo);
            dispatch(endGetUserInfoLocal(userInfo));
        }).catch(err => {
            console.log('[Get user info local fail] '+ err);
        });
    };
}

function startGetUserInfoLocal(){
    return {
        type: '@USER_INFO/START_GET_USER_INFO_LOCAL'
    };
}

function endGetUserInfoLocal(userInfo){
    return {
        type: '@USER_INFO/END_GET_USER_INFO_LOCAL',
        userInfo
    };
}

export function calculatePrepareTime(eventId, userId, userRecommandTime){
    return (dispatch, getState) => {
        return calculatePrepareTimeFromApi(eventId, userId).then(res => {
            if(res === "") throw "res is empty";
            if(res.confirm == 0) return;

            const datetime = parseInt(res[0].datetime);
            const arrivetime = parseInt(res[0].arrivetime);

            var time = arrivetime - datetime;
            if(time > 0){
                // late to the appointment
                //TODO: update formula
                userRecommandTime += Math.round(time/5);
            }else {
                // arrive ealier than the datetime
                userRecommandTime -= Math.round(time/10);
            }
            dispatch(updateUserRecommandTime(userRecommandTime));
        }).catch(err => {
            console.log('[Get prepare time fail] '+ err);
        });
    };
}

function updateUserRecommandTime(userRecommandTime){
    return {
        type: '@USER_INFO/UPDATE_USER_RECOMMAND_TIME',
        userRecommandTime,
    };
}
