/*------------cashflowItem------------*/

export function toggleCashFlowListItem(id) {
    return {
        type: '@CASHFLOWITEM/TOGGLE_CASHFLOW_LISTITEM',
        id
    }
}

/*------------invitationsItem------------*/

export function toggleInvitationsListItem(id) {
    return {
        type: '@INVITATIONSITEM/TOGGLE_INVITATIONS_LISTITEM',
        id
    }
}

/*------------unconfirmedItem------------*/

export function toggleUnconfirmedListItem(id) {
    return {
        type: '@UNCONFIRMEDITEM/TOGGLE_UNCONFIRMEDITEM_LISTITEM',
        id
    }
}