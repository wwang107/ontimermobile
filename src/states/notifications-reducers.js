/*------------cashflowItem------------*/

const initCashFlowItemState = {
    collapsed: {},
};

export function cashflowItem(state = initCashFlowItemState, action) {
    switch (action.type) {
        case '@CASHFLOWITEM/TOGGLE_CASHFLOW_LISTITEM':
            return {
                ...state,
                collapsed: {
                    [action.id]: !state.collapsed[action.id]
                },
            };
        default:
            return state;
    }
}

/*------------invitationsItem------------*/

const initInvitationsItemState = {
    collapsed: {}
};

export function invitationsItem(state = initInvitationsItemState, action) {
    switch (action.type) {
        case '@INVITATIONSITEM/TOGGLE_INVITATIONS_LISTITEM':
            return {
                ...state,
                collapsed: {
                    [action.id]: !state.collapsed[action.id]
                }
            };
        default:
            return state;
    }
}

/*------------unconfirmedItem------------*/

const initUnconfirmedItemState = {
    collapsed: {}
};

export function unconfirmedItem(state = initUnconfirmedItemState, action) {
    switch (action.type) {
        case '@UNCONFIRMEDITEM/TOGGLE_UNCONFIRMEDITEM_LISTITEM':
            return {
                ...state,
                collapsed: {
                    [action.id]: !state.collapsed[action.id]
                }
            };
        default:
            return state;
    }
}