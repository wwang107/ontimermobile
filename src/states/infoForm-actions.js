import {getFriendList} from "../api/fb.js";
import {NetInfo} from "react-native";
import {getEventInfoById} from "../api/events.js";
import {timeConverter} from "../api/utilities.js"
function setDepositRange(value){
    return {
        type: "@INFO_FORM/SET_DEPOSIT_RANGE",
        value: value,
    }
}

function dangerMaxDeposit(value){
    return {
        type: "@INFO_FORM/DANGER_MAX_DEPOSIT",
        value: value,
    }
}

function dangerMinDeposit(value){
    return {
        type: "@INFO_FORM/DANGER_MIN_DEPOSIT",
        value: value,
    }
}

function setFriendList(friendList){
    return {
        type: "@INFO_FORM/SET_FRIEND_LIST",
        friendList: friendList,
    }
}

export function setFormJoin(){
    return {
        type: "@INFO_FORM/SET_FORM_JOIN",
    }
} 

export function setFormCreate(){
    return {
        type: "@INFO_FORM/SET_FORM_CREATE",
    }
}

export function setForm(formType){
    return {
        type: "@INFO_FORM/SET_FORM",
        formType: formType,
    }
}

export function setValue(value){
    return {
        type: "@INFO_FORM/SET_VALUE",
        value: value,
    }
}

//will parse index to onFocus
export function mountOnFocusUseInScrollView(onFocus){
    return {
        type: "@INFO_FORM/MOUNT_ONFOCUS_USE_IN_SCROLL_VIEW",
        onFocus: onFocus,
    }
}
export function syncSuggestionFriend(){
    return (dispatch, getState)=>{
        return NetInfo.isConnected.fetch().then((isConnect)=>{
            // console.log(isConnect);
            if(isConnect){
                return getFriendList().then((friendList)=>{
                    console.log(friendList);
                    dispatch(setFriendList(friendList));

                });
            } else {
                alert("Please make sure you are connect to Internet");
            }
        });
        
    }
}

export function checkDeposit(value){
    return (dispatch, getState)=>{
        let {infoForm} = getState();
        // console.log(infoForm);
        let old_max = Number(infoForm.infoValue.maxDeposit);
        let old_min = Number(infoForm.infoValue.minDeposit);
        if(value.minDeposit!==undefined && value.maxDeposit!==undefined) {
            if(old_max!==Number(value.maxDeposit)){
                if(Number(value.minDeposit) <= Number(value.maxDeposit)){
                    dispatch(setDepositRange(value));
                }else{
                    dispatch(dangerMaxDeposit(value));
                }
            }else if(old_min!==Number(value.minDeposit)){
                if(Number(value.minDeposit) <= Number(value.maxDeposit)){
                    dispatch(setDepositRange(value));
                }else{
                    dispatch(dangerMinDeposit(value));
                }
            }
        }
    }
}

export function changeFormByPermission(permission){
    return (dispatch, getState)=>{
       // if(permission==="join"){
       //      dispatch(setFormJoin());
       // } else if(permission==="create"){
       //      dispatch(setFormCreate());
       // }
       dispatch(setForm(permission));
    }
}


function setJoinFormContent(value) {
    return {
        type: "@INFO_FORM/SET_JOIN_FORM_CONTENT",
        value: value,
    };
}

export function setEventInfoById(eventId) {
    return (dispatch, getState) => {
        // debugger
        if(getState().infoForm.permission==="join"){
            getEventInfoById(eventId).then((res)=>{
                console.log(res);
                if(res){
                    let value = {
                        eventId: eventId, //hide only to store value
                        eventName: res.eventname,
                        hostName: res.hostername,
                        eventDateTime: timeConverter(res.datetime, 2),
                        dateTime: res.datetime,
                        activityAddress: res.address,
                        currency: res.currency.toString(),
                        maxDeposit: res.maxdeposit.toString(),
                        minDeposit: res.mindeposit.toString(),
                        deposit: res.mindeposit,
                        about: res.about,
                    };
                    dispatch(setJoinFormContent(value));
                }
                else{
                    let value = {};
                    dispatch(setJoinFormContent(value));
                }
                
            });
        }
    }
}