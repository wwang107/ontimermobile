
function initialState() {
    return{
        deviceLocation:{lat:'na',long:'na'},
        events:{},
        loadingEvent: false,
        loadingDeviceLocation: false
    }
};

export function event(state = initialState(), action){
    switch (action.type){
        case '@HOME/LOADING_DEVICE_LOCATION':
            return {
                ...state,
                loadingDeviceLocation: true
            };

        case '@HOME/LOADING_EVENT':
            return {
                ...state,
                loadingEvent: true
            };
        
        case '@HOME/END_GET_EVENTS':
            return {
                ...state,
                events: action.events
            };

        case '@HOME/RESET_EVENT':
            return {
                ...state,
                events: action.events
            };

        case '@HOME/SET_EVENT':
        return {
            ...state,
            events: action.events
        };

        case '@HOME/END_LOADING_EVENT':
            return {
                ...state,
                loadingEvent:false
            };

        case '@HOME/END_LOADING_DEVICE_LOCATION':
            return {
                ...state,
                loadingDeviceLocation: false
            };
        
        case '@HOME/TRAVEL_TIME':
            return {
                ...state
            }

        case '@HOME/SET_LOCATION':
            return {
                ...state,
                deviceLocation: {lat:action.lat,
                                long:action.long}
            }

        case '@HOME/GET_ETA':
            return {
                ...state,
                eta:action.eta
            }

        default:
            return {
                ...state
            };
    }
}