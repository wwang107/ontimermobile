export function backListenerIsOn(){
  return {
      type: '@FLAG_CONTROLER/BACK_LISTENER_IS_ON'
  };
}

export function toggleAllNotification(){
  return {
      type: '@FLAG_CONTROLER/TOGGLE_ALL_NOTIFICATION'
  };
}

export function toggleNewInviteNotification(){
  return {
      type: '@FLAG_CONTROLER/TOGGLE_NEW_INVITE_NOTIFICATION'
  };
}

export function toggleUpcomingNotification(){
  return {
      type: '@FLAG_CONTROLER/TOGGLE_UPCOMING_NOTIFICATION'
  };
}
