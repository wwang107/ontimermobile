import {sliderTemplate, participantTemplate, participantTransformer, infoStructCreate, infoStructJoin, getInitialState} from "../components/EventFormScreenSubpage/infoField"


function initialInfoState() {
    let initState = getInitialState("create");
    let newState = {
        infOptions:{...initState.options},
        formType: initState.formType,
        infoValue: {...initState.value},
        permission: initState.permission,
    };
    return newState;
};


//import {infoForm as infoFormReducer} from './state/infoForm-reducers'; in app.js
// so use connect(...{...state.infoFormReducer};...);
export function infoForm(state=initialInfoState(), action){
    // console.log(action);
    // console.log(state)
    // debugger
    switch (action.type) {

        case "@INFO_FORM/SET_DEPOSIT_RANGE":
            return {
                ...state, 
                infOptions: {
                    ...state.infOptions,
                    fields: {
                        ...state.infOptions.fields, 
                        maxDeposit:{
                            ...state.infOptions.fields.maxDeposit,
                            hasError: false,
                        },
                        minDeposit:{
                            ...state.infOptions.fields.minDeposit,
                            hasError: false,
                        },
                        deposit:{
                            ...state.infOptions.fields.deposit,
                            help: false,
                            config:{
                                ...state.infOptions.fields.deposit.config,
                                minimumValue: action.value.minDeposit,
                                maximumValue: action.value.maxDeposit,
                                disabled: false,
                            }
                        },
                    }
                },
                infoValue: {
                    ...state.infoValue,
                    deposit: action.value.minDeposit,
                }
            };

        case "@INFO_FORM/DANGER_MAX_DEPOSIT":
            return {
                ...state, 
                infOptions: {
                    ...state.infOptions,
                    fields: {
                        ...state.infOptions.fields, 
                        maxDeposit:{
                            ...state.infOptions.fields.maxDeposit,
                            hasError: true,
                            error: "should be higher than Min desposite"
                        },
                        deposit:{
                            ...state.infOptions.fields.deposit,
                            config:{
                                ...state.infOptions.fields.deposit.config,
                                disabled: true,
                            }
                        },
                    }
                },
                // infoValue: action.value
            };
        case "@INFO_FORM/DANGER_MIN_DEPOSIT":
            return {
                ...state, 
                infOptions: {
                    ...state.infOptions,
                    fields: {
                        ...state.infOptions.fields, 
                        minDeposit:{
                            ...state.infOptions.fields.minDeposit,
                            hasError: true,
                            error: "should be lower than Max desposite"
                        },
                        deposit:{
                            ...state.infOptions.fields.deposit,
                            config:{
                                ...state.infOptions.fields.deposit.config,
                                disabled: true,
                            }
                        },
                    }
                },
                // infoValue: action.value
            };
        case "@INFO_FORM/SET_VALUE":
            return {
                ...state,
                infoValue: {
                    ...state.infoValue, //preserve the value set by code
                    ...action.value        //add the value set by user
                }
            }
        case "@INFO_FORM/SET_FRIEND_LIST":
            return {
                ...state, 
                infOptions: {
                    ...state.infOptions,
                    fields: {
                        ...state.infOptions.fields, 
                        participant:{
                            ...state.infOptions.fields.participant,
                            config:{
                                ...state.infOptions.fields.participant.config,
                                suggestions : action.friendList,
                            },
                        }, 
                    },
                },
            }
        // case "@INFO_FORM/SET_FORM_JOIN":
        //     let initState = getInitialState("join");
        //     return {
        //         ...state,
        //         infOptions:{
        //             ...initState.options,
        //         },
        //         formType: initState.formType,
        //         infoValue: initState.value,
        //         permission: initState.permission,
        //     };
        // case "@INFO_FORM/SET_FORM_CREATE":
        //     let initState = getInitialState("create");
        //     return {
        //         ...state,
        //         infOptions:{
        //             ...initState.options,
        //         },
        //         formType: initState.formType,
        //         infoValue: initState.value,
        //         permission: initState.permission,
        //     };
        case "@INFO_FORM/SET_FORM":
            let initState = getInitialState(action.formType);
            let newState = {
                infOptions:{...initState.options},
                formType: initState.formType,
                infoValue: {...initState.value},
                permission: initState.permission,
            };
            if(initState.permission==="create"){
                console.log("create");
                return initialInfoState();
            }
            // debugger
            return newState;
        case "@INFO_FORM/SET_JOIN_FORM_CONTENT":
            // debugger
            return {
                ...state,
                infoValue: action.value,
                infOptions: {
                    ...state.infOptions,
                    fields:{
                        ...state.infOptions.fields,
                        deposit:{
                            ...state.infOptions.fields.deposit,
                            config:{
                                ...state.infOptions.fields.deposit.config,
                                maximumValue: action.value.maxDeposit,
                                minimumValue: action.value.minDeposit,
                                disabled: false,
                            }
                        },
                    },
                },
            }
        case "@INFO_FORM/MOUNT_ONFOCUS_USE_IN_SCROLL_VIEW":
        // debugger
            return {
                ...state, 
                infOptions: {
                    ...state.infOptions,
                    fields: {
                        ...state.infOptions.fields, 
                        activityAddress:{
                            ...state.infOptions.fields.activityAddress,
                            onFocus: ()=>{action.onFocus(0);},
                        }, 
                        participant:{
                            ...state.infOptions.fields.participant,
                            onFocus: ()=>{action.onFocus(1);},
                        }, 
                    },
                },
            }
        default:
            return state;
    }

}