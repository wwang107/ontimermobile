const initUserInfo = {
  userId: "user id",
  userName: "User Name",
  userPicture: "https://cdn1.iconfinder.com/data/icons/unique-round-blue/93/user-256.png",
  userCoins: 0,
  accessPermmision: 0,
  userPhoneNumber: "0912345678",
  attendEvent: [],
  isEdited: false,
  userEmail: '',
  userHomeLocation: '',
  userGender: '',
  userPrepareTime: 0,
  userRecommandTime: 0,
  userPaymentList: [],
  fbUserId: '',
  fbUserName: '',
};
export function userInfo(state = initUserInfo, action) {
    switch (action.type) {
        case '@USER_INFO/UPDATE_USER_RECOMMAND_TIME':
            return {
                ...state,
                userRecommandTime: action.userRecommandTime,
            };
        case '@USER_INFO/START_GET_USER_INFO_LOCAL':
            return {
                ...state,
            };
        case '@USER_INFO/END_GET_USER_INFO_LOCAL':
            return {
                ...state,
                fbUserId: action.userInfo.fbUserId,
                fbUserName: action.userInfo.fbUserName,
                userName: action.userInfo.userName,
                userPicture: action.userInfo.userPicture ,
                userPhoneNumber: action.userInfo.userPhoneNumber,
                userCoins: action.userInfo.userCoins,
                userEmail: action.userInfo.userEmail,
                userHomeLocation: action.userInfo.userHomeLocation,
                userGender: action.userInfo.userGender,
                userPrepareTime: action.userInfo.userPrepareTime,
                userPaymentList: action.userInfo.userPaymentList,
            };
        case '@USER_INFO/STORE_USER_INFO_LOCAL':
            return {
                ...state,
            };
        case '@USER_INFO/SET_USER_PAYMENT':
            return {
                ...state,
                userPaymentList: action.userPaymentList,
            };
        case '@USER_INFO/START_GET_USER_PAYMENT':
            return {
                ...state,
            };
        case '@USER_INFO/END_GET_USER_PAYMENT':
            return {
                ...state,
                userPaymentList: action.userPaymentList,
            };
        case '@USER_INFO/UPDATE_USER_NAME':
            return {
                ...state,
                userName: action.userName,
            };
        case '@USER_INFO/UPDATE_USER_PHONE_NUMBER':
            return {
                ...state,
                userPhoneNumber: action.userPhoneNumber,
            };
        case '@USER_INFO/UPDATE_USER_EMAIL':
            return {
                ...state,
                userEmail: action.userEmail,
            };
        case '@USER_INFO/UPDATE_USER_HOME_LOCATION':
            return {
                ...state,
                userHomeLocation: action.userHomeLocation,
            };
        case '@USER_INFO/UPDATE_USER_GENDER':
            return {
                ...state,
                userGender: action.userGender,
            };
        case '@USER_INFO/UPDATE_USER_PREPARE_TIME':
            return {
                ...state,
                userPrepareTime: action.userPrepareTime,
            };
        case '@USER_INFO/START_FETCH_USER_INFO':
            return {
                ...state
            };
        case '@USER_INFO/END_FETCH_USER_INFO':
            return {
                ...state,

                userName: action.userInfo.username,
                userPicture: action.userInfo.userpicture ,
                userPhoneNumber: action.userInfo.userphonenumber,
                userCoins: action.userInfo.usercoins,
                userPrepareTime: action.userInfo.preparetime,
                userRecommandTime: action.userInfo.recommendtime,
            };
        case '@USER_INFO/START_EDIT_USER_INFO':
            return {
                ...state
            };

        case '@USER_INFO/START_UPLOAD_USER_PREPARE_TIME':
            return {
                ...state
            };

        case '@USER_INFO/END_EDIT_USER_INFO':
            return {
                ...state,
                userName: action.userInfo.username,
                userPicture: action.userInfo.userpicture ,
                userPhoneNumber: action.userInfo.userphonenumber,
                userCoins: action.userInfo.usercoins,
                userPrepareTime: action.userInfo.preparetime,
            };
        case '@USER_INFO/START_CREATE_USER':
            return {
                ...state
            };
        case '@USER_INFO/END_CREATE_USER':
            console.log(action);
            return {
                ...state,
                userName: action.userInfo.username,
                userPicture: action.userInfo.userpicture ,
                userPhoneNumber: action.userInfo.userphonenumber,
                userCoins: action.userInfo.usercoins,
            };
        case '@USER_INFO/SET_IS_EDITED':
            return {
                ...state,
                isEdited: true,
            };
        case '@USER_INFO/UNSET_IS_EDITED':
            return {
                ...state,
                isEdited: false,
            };
        case 'USER_INFO/SET_FB_INFO':
            return {
                ...state,
                fbUserId: action.info.id, //should be removed
                userId: action.info.id,
                fbUserName: action.info.name,
            };
        default:
            return state;
    }
}
