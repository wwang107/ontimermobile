import React from 'react';
import { View, StyleSheet, Image, BackHandler, NetInfo} from 'react-native';
import { Container, Content, Body, Text, Left, Right,
  Tab, Tabs, TabHeading,
  Card, CardItem} from 'native-base';

import { NavigationActions } from 'react-navigation';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getUserInfo, editUserInfo, uploadUserPrepareTime,unsetIsEdited} from '../states/user-actions.js'

import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';

import SettingTab from "./AccountScreenSubpage/SettingTab.js";
import AchievementTab from "./AccountScreenSubpage/AchievementTab.js";
import AttendEventTab from "./AccountScreenSubpage/AttendEventTab.js";
import StatisticTab from "./AccountScreenSubpage/StatisticTab.js";

import {getCurrentRouteName} from "../utility/navUtility.js";
import {getUserPict} from "../api/fb.js";

const uri = "https://cdn1.iconfinder.com/data/icons/unique-round-blue/93/user-256.png";
const uri_bg = "https://thumbs.dreamstime.com/t/wood-metallic-texture-background-rustic-banner-blue-tones-51204566.jpg";

const ownerId = '103081045'; //debug

const generalWordSize = 30;

export class AccountScreen extends React.Component {

    static propTypes = {
        userId: PropTypes.string.isRequired,
        userName: PropTypes.string,
        userPicture: PropTypes.string,
        coins: PropTypes.number,
        accessPermmision: PropTypes.number,
        userPhoneNumber: PropTypes.string,
        attendEvent: PropTypes.arrayOf(PropTypes.object)
    };

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.getParam('title', 'User ID')}`,
    });



    constructor(props) {
        super(props);

        this.state={
            isFocused: false,

        };

        this._didFocus = this._didFocus.bind(this);
        this._willBlur = this._willBlur.bind(this);
    }

    _didFocus = () => {
        this.setState({isFocused: true});
        
        NetInfo.isConnected.fetch().then(isConnected => {
            console.log('First, is ' + (isConnected ? 'online' : 'offline'));
            if(isConnected){
                if (this.props.isEdited){
                    console.log(this.props.userPrepareTime);
                    console.log("user info has been modified");
                    const userInfo = {
                        userId: this.props.userId,
                        userName: this.props.userName,
                        userPicture: this.props.userPicture,
                        userCoins: this.props.userCoins,
                        userPhoneNumber: this.props.userPhoneNumber,
                        userPrepareTime: this.props.userPrepareTime}
                    
                    console.log('I am in did Focus!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
                    console.log(this.state);
                    // toggle isEdited if upload userinfo successfully
                    //TODO: upload preparetime
                    
                    this.props.dispatch(editUserInfo(userInfo));
                    this.props.dispatch(uploadUserPrepareTime(userInfo.userPrepareTime, userInfo.userId));
                }
                else {
                    this.props.dispatch(getUserInfo(this.props.userId));
                }
            }
        });
    }

    _willBlur = () => {
        console.log('I am in willBlur!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        console.log(this.state);
        this.setState({isFocused: false});
    }


    androidHardwareBackHandler = () => {
        const mainScreens = ['Home', 'Add Event', 'Notification', 'Account', 'Login'];

        let {routes} = this.props;
        if(!mainScreens.includes(getCurrentRouteName(routes))){
            this.props.dispatch(NavigationActions.back());
            return true;
        }
        return false;
    }

    componentDidMount(){
        this.subs = [
            this.props.navigation.addListener("didFocus", this._didFocus),
            this.props.navigation.addListener("willBlur", this._willBlur),
        ];
        this.props.dispatch(getUserInfo(this.props.userId));

        // set title to owner_id
        this.props.navigation.setParams({title: this.props.userId});


        // BackHandler.addEventListener('hardwareBackPress', this.androidHardwareBackHandler);

    }

    

    componentWillUnmount(){
        this.subs.forEach(sub => sub.remove());
        // BackHandler.removeEventListener('hardwareBackPress', this.androidHardwareBackHandler);
    }

    render() {
        let {userId, userName, userPicture, userCoins, 
            accessPermmision, userPhoneNumber, attendEvent, userRecommandTime} = this.props;
            userName = userName.length > 15 ? userName.split(" ")[0] : userName;
        return (
            <Container>
                <View style={styles.accountPage}>
                    <View style={styles.accountProfileBox}>
                        <View style={styles.accountMainInfoBox}>
                            <View style={styles.accountPhotoBox}>
                                <Image cover resizeMethod={"scale"} style={styles.accountPicture} source={{uri: getUserPict(userId, "large")}}/>
                            </View>
                            <View style={styles.accountNameBox}>
                                <Text style={styles.strongWords}>{userName}</Text>
                            </View>
                        </View>

                        <View style={styles.accountMinorInfoBox}>
                            <Card>
                              <CardItem style={styles.generalBox} bordered>
                                  <Entypo name="phone" size={30} />
                                  <Text style={styles.generalWords}>{userPhoneNumber}</Text>
                              </CardItem>

                              <CardItem style={styles.generalBox} bordered>
                                  <MaterialCommunityIcons name="coin" size={30} />
                                  <Text style={styles.generalWords}>{userCoins}</Text>
                              </CardItem>

                              {/*TODO: layout, add unit, convert unit to minute*/}
                              <CardItem style={styles.generalBox}>
                                  <View style={{ marginRight: 10}}>
                                    <Ionicons name="md-time" size={30} />
                                  </View>
                                  <Text style={styles.generalWords}>{userRecommandTime}</Text>
                              </CardItem>
                            </Card>
                        </View>
                    </View>
                    <Tabs style={styles.accountDetailBox}>
                    {/*
                        <Tab heading={ <TabHeading><Entypo name="list" size={30} /></TabHeading>}>
                            <AttendEventTab />
                        </Tab>
                        <Tab heading={ <TabHeading><EvilIcons name="chart" size={30} /></TabHeading>}>
                            <StatisticTab />
                        </Tab>
                        <Tab heading={ <TabHeading><EvilIcons name="trophy" size={30} /></TabHeading>}>
                            <AchievementTab />
                        </Tab>
                    */}
                        <Tab heading={ <TabHeading><EvilIcons name="gear" size={30}/></TabHeading>}>
                            <SettingTab/>
                        </Tab>
                    </Tabs>
                </View>
            </Container>
        );
    }


}


const styles = StyleSheet.create({
    accountPage : {
        flex: 1,
        justifyContent: 'center'
    },

    // in accountPage
    accountProfileBox : {
        flex: 3,
        flexDirection: 'row',
    },

    accountDetailBox : {
        flex: 6,

    },

    // in accountProfileBox
    accountMainInfoBox : {
        flex: 3,
        // backgroundColor: 'red'

    },

    accountMinorInfoBox : {
        flex: 5,
        // backgroundColor: 'green'

    },

    // in accountMainInfoBox
    backGroundImageBox : {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        // backgroundColor: '#eee'
    },

    accountPhotoBox : {
        flex: 7,
        justifyContent: 'center',
        alignItems: 'center'
        // backgroundColor: 'blue'

    },

    accountNameBox : {
        flex: 2,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'rgba(255,255,255,0.5)',
        // marginLeft: 60,
        // marginRight: 60,
        marginBottom: 10,
        borderRadius: 20,
        // backgroundColor: 'purple'

    },

    // in backGroundImageBox
    backGroundImage : {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',

    },

    // in accountPhotoBox
    accountPicture : {
        width: 120,
        height: 120,
        borderRadius:100,
        borderWidth: 2,
        borderColor:'rgba(255,255,255,0.5)',
    },


    // utility
    imageOverlay : {
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0, 0, 0, 0)',

    },

    strongWords : {
        fontWeight: 'bold',
        fontSize: 20,
    },

    weakWords : {
        color: 'gray',
    },

    generalWords : {
        marginLeft: 5,
        fontSize: 20,
    },

    generalBox : {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
});

export default connect((state, ownProps) => ({
    ...state.userInfo,
    ...state.nav,
}))(AccountScreen);
