import React, {Component} from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
    View,
    Button,
	TouchableHighlight,
	ImageBackground
} from 'react-native';
import {LoginButton, AccessToken, GraphRequestManager, GraphRequest} from 'react-native-fbsdk';
import {setFireTime, notifyEventStart, notifyOneDayBefore} from '../api/pushNotification'

export default class testScreen extends Component {

    handleOnPress = () => {
        console.log("Alarm!!");
        //let eventTime = "2018-06-17T23:00:00+08:00";
        let eventTime = 1529247600; // sec
        let eventId = 14;
        let userId = 10;
        let alarmTime = 1;
        let userAuthority = "hoster";
        setFireTime(userId, eventId, eventTime, alarmTime, userAuthority);
    };

    render() {
        return (

            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>Press the button to see push Notification</Text>
                <Button title={'Press Me'} onPress={this.handleOnPress}/>
            </View>
        );
    }

}
