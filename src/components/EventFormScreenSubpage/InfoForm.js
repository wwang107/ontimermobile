import React from "react";
import { Text, View, ScrollView, StyleSheet, Slider, TouchableHighlight} from "react-native";
import * as tcomb from "tcomb-form-native";
import {connect} from "react-redux";
import {setValue, checkDeposit, syncSuggestionFriend, changeFormByPermission, setEventInfoById, mountOnFocusUseInScrollView} from "../../states/infoForm-actions";
import {MoneyIntervel, Currency} from "./infoField"
import AutoTags from './AutoTags';

const styles = StyleSheet.create({
    contentContainer: {
    paddingVertical: 20,
    justifyContent: "center",
    padding: 20,
    backgroundColor: "#ffffff",
  },

});

//TODO measure the component position
const hardCodePos = [220, 1000];

class InfoForm extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            indexOfDynamicLenCom: -1,
            contentHeight: 0,
            contentOffsetY: 0,
        }
        this.getValue = this.getValue.bind(this);
    }
    

    componentDidMount() {
        
        
        this.props.onRef(this);
        
        // all dispatched action realted to tcomb-form should right in formDidMount()
        this.props.dispatch(syncSuggestionFriend());//this is an exception,cause write in componentDidMount will make it run faster

    }
    
    //put all action related to tcomb-form in this function
    formDidMount(permission){
        if(permission==="create"){
            this.props.dispatch(mountOnFocusUseInScrollView((index)=>{this.onFocusUseInScrollView(index);}));
        }
    }

    componentWillUnmount() {
        this.props.onRef(undefined);
    }
    onChange(value){
        // console.log(value);
        if(this.props.permission==="create")
            this.props.dispatch(checkDeposit(value));
        this.props.dispatch(setValue(value));

    }
    
    getValue(){
        var value = this.refs.infoForm.getValue();
        if(value && this.props.permission==="create"){
            value = {
                ...value,
                eventDateTime: new Date(value.eventDateTime).getTime() / 1000,
            }
            if(value.participant){
                value={
                    ...value,
                    participant: JSON.parse(value.participant),
                };
            }
        }else if(value===null && this.props.permission==="join"){
            this.refs.scrollView.scrollToEnd();
        }
        return value;
    }
    
    changePermission(permission){
        //this will reset form
        this.props.dispatch(changeFormByPermission(permission));
        this.formDidMount(permission);
    }

    searchEventById(eventId){
        // if(this.props.permission==="join"){
            this.props.dispatch(setEventInfoById(eventId));
        // }else{
        //     console.log("error in searchEventById in InfoForm.js");
        // }
    }

    onFocusUseInScrollView(index){
        this.setState({
            ...this.state,
            indexOfDynamicLenCom: index,
        });
    }
    
    // handleScroll(event){
    //     // console.log("ScrollView: ");
    //     // console.log(event);
    //     console.log(event.nativeEvent.contentOffset.y);
    // }
    
    

    handleSizeChange(contentWidth, contentHeight){
        let newContentOffsetY = this.state.contentOffsetY + contentHeight-this.state.contentHeight;
        newContentOffsetY = newContentOffsetY >= 0 ? this.state.contentOffsetY : 0;
        this.setState({
            ...this.state,
            contentHeight: contentHeight,
            contentOffsetY: newContentOffsetY
        });
        if(this.state.contentHeight < contentHeight && this.state.contentHeight>0){
            this.refs.scrollView.scrollTo({x:0, y:(hardCodePos[this.state.indexOfDynamicLenCom]+this.state.contentOffsetY), animated:true});
        }
        
        

    }
    render() {
      
        return (
            <ScrollView 
                ref = "scrollView"
                contentContainerStyle={styles.contentContainer} 
                // onScroll={(event)=>{this.handleScroll(event)}}
                onContentSizeChange={(contentWidth, contentHeight)=>{
                this.handleSizeChange(contentWidth, contentHeight);

            }}>
                <tcomb.form.Form 
                    ref = "infoForm"
                    type={this.props.formType} 
                    options={this.props.infOptions} 
                    value={this.props.infoValue} 
                    onChange={(value)=>{this.onChange(value)}}
                />
            </ScrollView>

        );
    }
}



export default connect((state)=>{
    return {
        ...state.infoForm,
    };
})(InfoForm);
