import React from 'react';
import { View, Text, Slider, StyleSheet} from 'react-native';
import * as tcomb from "tcomb-form-native";
// import AutoTags from 'react-native-tag-autocomplete';
import AutoTags from './AutoTags';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export const MoneyIntervel = tcomb.enums({
    "30":"30",
    "50": "50",
    "100": "100",
    "150": "150",
    "200": "200",
    "500": "500",
});

export const Currency = tcomb.enums({
    "NTD": "NTD",
    "Virtual Coin" : "Virtual Coin"
});


//TODO: solve the color problem
const participantStyle = StyleSheet.create({
        inputContainerStyle: {
        borderRadius: 0,
        paddingLeft: 5,
        height: 40,
        width: 300,
        justifyContent: "center",
        borderColor: "transparent",
        alignItems: "stretch",
        backgroundColor: "#efeaea"
    },
    tagStyles: {
        flexDirection: "row",
        flexWrap: "wrap",
        alignItems: "flex-start",
        backgroundColor: "#ffffff",
        width: 300
    }
});

export var participantTransformer = {
    format: (value)=>{
        return Array.isArray(value) ? JSON.stringify(value):value;
    },
    // parse: (str)=>{
    //     return (typeof str === "string") ? JSON.parse(str) : str;
    // },
    parse: (str)=>{
        return str;
    },
};



//(deprecate)https://github.com/JoeRoddy/react-native-tag-autocomplete/blob/master/index.js
//Already change to local one
export function participantTemplate (locals) {
    if (locals.hidden) {
        return null;
    }

    var stylesheet = locals.stylesheet;
    var formGroupStyle = stylesheet.formGroup.normal;
    var controlLabelStyle = stylesheet.controlLabel.normal;
    var checkboxStyle = stylesheet.checkbox.normal;
    var helpBlockStyle = stylesheet.helpBlock.normal;
    var errorBlockStyle = stylesheet.errorBlock;
    var participantConfig = locals.config;

    if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        checkboxStyle = stylesheet.checkbox.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }
    
    var label = locals.label ? (<Text style={controlLabelStyle}>{locals.label}</Text>) : null;
    var help = locals.help ? (<Text style={helpBlockStyle}>{locals.help}</Text>) : null;
    var error = locals.hasError && locals.error ? (<Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text>) : null;
    var autoFocus = locals.autoFocus ===true ? true : false;
    var tags = participantConfig.stringifyInput? (Array.isArray(locals.value)? locals.value.slice() : JSON.parse(locals.value)) : locals.value.slice();
    // debugger
    return (
        <View style={formGroupStyle}>
        {label}
            <AutoTags
                filterTagsSelected={participantConfig.filterTagsSelected}
                suggestions={participantConfig.suggestions}
                tagsSelected={tags}
                handleAddition={(suggestion)=>{
                    // let tags = locals.value;
                    // console.log(tags.concat([suggestion]));
                    let returnTags = participantConfig.stringifyInput? JSON.stringify(tags.concat([suggestion])) : tags.concat([suggestion]);
                    // console.log(returnTags);
                    locals.onChange(returnTags);
                }}
                handleDelete={(index)=>{
                    // let tags = locals.value.slice();
                    let returnTags = participantConfig.stringifyInput? JSON.stringify(tags.splice(index, 1)) : tags.splice(index, 1);
                    locals.onChange(returnTags);
                }} 
                placeholder="Add a contact.." 
                autoFocus={autoFocus}
                onFocus = {locals.onFocus}
                // inputContainerStyle = {participantStyle.inputContainerStyle}
                // tagStyles = {participantStyle.tagStyles}
            />
        {help}
        {error}
        </View>
    );   
}

export function sliderTemplate (locals) {
    if (locals.hidden) {
        return null;
    }

    var stylesheet = locals.stylesheet;
    var formGroupStyle = stylesheet.formGroup.normal;
    var controlLabelStyle = stylesheet.controlLabel.normal;
    var checkboxStyle = stylesheet.checkbox.normal;
    var helpBlockStyle = stylesheet.helpBlock.normal;
    var errorBlockStyle = stylesheet.errorBlock;
    var sliderConfig = locals.config;

    if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        checkboxStyle = stylesheet.checkbox.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }
    
    var label = locals.label ? (<Text style={controlLabelStyle}>{locals.label}</Text>) : null;
    var help = locals.help ? (<Text style={helpBlockStyle}>{locals.help}</Text>) : null;
    var error = locals.hasError && locals.error ? (<Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text>) : null;
    

    //deal with value range change
    var maximumValue = Number(sliderConfig.maximumValue);
    var minimumValue = Number(sliderConfig.minimumValue);
    var displayValue = Number(locals.value);
    // console.log(`${maximumValue} ${minimumValue} ${displayValue}`)
    if(maximumValue!==NaN && displayValue > maximumValue)
        displayValue = maximumValue;
    else if(minimumValue!==NaN && displayValue < minimumValue)
        displayValue = minimumValue;
    
    return (
        <View style={formGroupStyle}>
        {label}
            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 15}}>
                <Text>{displayValue}</Text>
            </View>  
            <View style={{marginBottom: 5}}>
                <Slider
                  maximumValue={maximumValue}
                  minimumValue={minimumValue}
                  step={sliderConfig.step}
                  disabled={sliderConfig.disabled}
                  value={displayValue}
                  onValueChange={(value) => locals.onChange(value)}/>
            </View>
        {help}
        {error}
        </View>
    );   
}

//https://github.com/FaridSafi/react-native-google-places-autocomplete/blob/master/GooglePlacesAutocomplete.js
export function addressTemplate(locals){
    if (locals.hidden) {
        return null;
    }

    var stylesheet = locals.stylesheet;
    var formGroupStyle = stylesheet.formGroup.normal;
    var controlLabelStyle = stylesheet.controlLabel.normal;
    var checkboxStyle = stylesheet.checkbox.normal;
    var helpBlockStyle = stylesheet.helpBlock.normal;
    var errorBlockStyle = stylesheet.errorBlock;
    var addressConfig = locals.config;

    if (locals.hasError) {
        formGroupStyle = stylesheet.formGroup.error;
        controlLabelStyle = stylesheet.controlLabel.error;
        checkboxStyle = stylesheet.checkbox.error;
        helpBlockStyle = stylesheet.helpBlock.error;
      }
    
    var label = locals.label ? (<Text style={controlLabelStyle}>{locals.label}</Text>) : null;
    var help = locals.help ? (<Text style={helpBlockStyle}>{locals.help}</Text>) : null;
    var error = locals.hasError && locals.error ? (<Text accessibilityLiveRegion="polite" style={errorBlockStyle}>{locals.error}</Text>) : null;
    var autoFocus = locals.autoFocus ===true ? true : false;
    // var onFocus = addressConfig.onFocus ;
    // debugger
    return (
    <GooglePlacesAutocomplete
          placeholder='Search'
          minLength={1} // minimum length of text to search
          autoFocus={false}
          returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
          listViewDisplayed='auto'    // true/false/undefined
          fetchDetails={true}
          renderDescription={row => row.description} // custom description render
          onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
            locals.onChange(data.description);
          }}
          textInputProps = {{
            onChangeText:(text)=>{
                locals.onChange(text);
            },
            onFocus: ()=>{
                if(locals.onFocus)
                    locals.onFocus();
            },
          }}
          getDefaultValue={() => ''}
          
          query={{
            // available options: https://developers.google.com/places/web-service/autocomplete
            key: 'AIzaSyCH_RiXhxchoQA8Lr0UHJFU7BOnsjWhGis',
            language: 'en', // language of the results
            types: 'geocode' // default: 'geocode'
          }}
          
          styles={{
            textInputContainer: {
              width: '100%'
            },
            description: {
              fontWeight: 'bold'
            },
            predefinedPlacesDescription: {
              color: '#1faadb'
            }
          }}
          
          currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
          currentLocationLabel="Current location"
          nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
          GoogleReverseGeocodingQuery={{
            // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
          }}
          GooglePlacesSearchQuery={{
            // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
            rankby: 'distance',
            types: 'food'
          }}

          // filterReverseGeocodingByTypes={['locality', , "political", 'administrative_area_level_5']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
          // predefinedPlaces={[homePlace, workPlace]}

          debounce={0} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
          // renderLeftButton={()  => <Image source={require(s'path/custom/left-icon')} />}
          // renderRightButton={() => <Text>Custom text after the input</Text>}
        />
    );
}
export const infoStructCreate = tcomb.struct({
    // test: tcomb.Boolean,
    eventName: tcomb.String, 
    eventDateTime: tcomb.Date,
    activityAddress: tcomb.String,
    currency: Currency,
    maxDeposit: MoneyIntervel,
    minDeposit: MoneyIntervel,
    deposit: tcomb.Number, //use custom template
    alarmTime: tcomb.Number,
    participant: tcomb.maybe(tcomb.String),
    about: tcomb.maybe(tcomb.String),
});

export const infoStructJoin = tcomb.struct({
    // test: tcomb.Boolean,
    eventId: tcomb.Number,
    eventName: tcomb.String,
    hostName: tcomb.String,
    eventDateTime: tcomb.String,
    dateTime: tcomb.String,
    activityAddress: tcomb.String,
    // currency: tcomb.String,
    maxDeposit: tcomb.String,
    minDeposit: tcomb.String,
    participant: tcomb.maybe(tcomb.String),
    about: tcomb.maybe(tcomb.String),
    deposit: tcomb.Number, //use custom template
    alarmTime: tcomb.Number,
});

export function getInitialState(formType){
    if(formType==="join"){
        return{
            options:{
                fields:{
                    eventId:{
                        hidden: true,
                    },
                    eventName: {
                        editable: false,
                    },
                    hostName: {
                        editable: false
                    },
                    eventDateTime: {
                        editable: false,
                    },
                    dateTime: {
                        hidden: true,
                    },
                    activityAddress: {
                        editable: false,
                    },
                    currency:{
                        editable: false,
                    },
                    maxDeposit:{
                        editable: false,
                    },
                    minDeposit:{
                        editable: false,
                    },
                    participant:{
                        editable: false,
                    },
                    about: {
                        editable: false,
                    },
                    deposit: {
                        config:{
                            minimumValue: "0",
                            maximumValue: "100",
                            step: 1,
                            disabled: true,
                        },
                        help: "Plase select deposit between max and min",
                        template: sliderTemplate,
                    },
                    alarmTime: {
                        label: "Alarm time (unit: mins)"
                    }
                },
            },
            value: null,
            permission: 'join',
            formType: infoStructJoin,
        };
    }else if(formType==="create"){
        return {
            options: {
                fields:{
                    eventName: {
                        placeholder: "Your help message here",
                        editable: true,
                    },
                    eventDateTime: {
                        mode: "datetime",
                        // format: (date) => String(date),
                    },
                    activityAddress: {
                        template: addressTemplate,
                    },
                    currency:{
                        nullOption: false,
                    },
                    maxDeposit:{
                        // options: [
                        //     {value: '500', text: '500', disabled: true},
                        //     {value: '300', text: '300'},
                        //     {value: '150', text: '150'},
                        //     {value: '100', text: '100'},
                        //     {value: '50', text: '50'},
                        //     {value: '30', text: '30'},
                            
                        // ],
                        nullOption: {value: "NaN", text: 'Choose Maximun Deposit'},
                        hasError: false,
                        error: "should be higher than Min desposite"
                    },
                    minDeposit:{
                        nullOption: {value: "NaN", text: 'Choose Minimum Deposit'},
                        // order: "desc",
                        hasError: false,
                        error: "should be lower than Max desposite",
                    },
                    deposit: {
                        config:{
                            minimumValue: 0,
                            maximumValue: 100,
                            step: 1,
                            disabled: true,
                        },
                        help: "Plase select max and min deposit first",
                        template: sliderTemplate,
                    },
                    participant:{
                        config:{
                            autoFocus: false,
                            suggestions : [ {name:'Mickey Mouse'}, {name:'Allen Chen'}, {name: 'Subina'}, {name: 'Micheal Wong'}, {name: 'Michel Wong'}],
                            filterTagsSelected: true,
                            stringifyInput: true,
                        },
                        template: participantTemplate,
                        transformer: participantTransformer,
                    },
                    alarmTime: {
                        label: "Alarm time (unit: mins)",
                    },
                },
            },
            value: {
                currency: "Virtual Coin",
                participant: [],
                // maxDeposit: '30',
            },
            permission: 'create',
            formType: infoStructCreate,    
        };
    }
}