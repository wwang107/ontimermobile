import React from "react";
import { Text, View, Modal, TouchableHighlight, Button, StyleSheet, NetInfo} from "react-native";
import InfoForm from "./InfoForm"
import {getGeoByAddress} from "../../api/googleMap";
import {createEvent as createEventApi, addMember as addMemberApi, addMembers as addMembersApi, invitemembers as invitemembersApi}  from "../../api/events";
import {connect} from "react-redux";
import {setFireTime} from "../../api/pushNotification";
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    createEventBtn:{
        // flex:1, 
        height: 44,
        width: 300,
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 10,
        borderColor : '#00d7df',
        justifyContent: "center",
        // backgroundColor: "transparent",

    },
    createEventBtnTxt:{
        fontSize:20, 
    },
});

export class CreateEventScreen extends React.Component {
    
    constructor(props){
        super(props);
        this.infoFormRef = React.createRef();
        this.state={
            buttonColor: "transparent",
        };
    }
    
    componentDidMount() {
        this.createEventForm.changePermission("create");
    }

    createEvent(){
        
        var value = this.createEventForm.getValue();
        // console.log(value);
        if(value){
            let url = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api/createvents";
            return NetInfo.isConnected.fetch().then((isConnect)=>{
                if(isConnect){
                    return getGeoByAddress(value.activityAddress).then((geo)=>{
                        if(geo){
                            let body = {
                                ...value,
                                "latitude":geo.lat,
                                "longitude":geo.lng,
                                "hoster": this.props.fbUserId,
                                "hostName": this.props.fbUserName,

                            };
                            createEventApi(body).then((res)=>{
                                alert(`create event success\nEvent ID: ${res.eventid}`);
                                console.log(res);
                                let userObj = {
                                    "eventId": res.eventid,
                                    "eventName": body.eventName,
                                    "deposit": body.deposit,
                                    "userName": this.props.fbUserName, //require
                                    "userId": this.props.fbUserId,
                                    "hostName": body.hostName,
                                    "hoster": this.props.fbUserId,
                                    "alarmTime":value.alarmTime,
                                    "confirm": 1,
                                    "datetime": res.datetime
                                };
                                
                                addMemberApi(userObj).then((res)=>{
                                    //hoster add notification
                                    // debugger

                                    setFireTime(userObj.userId, userObj.eventId, userObj.datetime, body.alarmTime, "hoster", {...body, "eventId": res.eventid});
                                    // debugger
                                    //hoster start to invite frineds
                                    let userObjTemlate = {
                                        "eventId": res.eventid,
                                        "eventName": value.eventName,
                                        "deposit": 0,
                                        // "userName": this.props.fbUserName, //require
                                        // "userId": this.props.fbUserId,
                                        "hoster": this.props.fbUserId,
                                        "hostName": value.hostName,
                                        "alarmTime":value.alarmTime,
                                        "confirm": 0,
                                        "datetime": res.datetime
                                    };

                                    let userObjs = [];
                                    for(let i=0 ; i < value.participant.length ; i++){
                                        let inviteUserObj = {
                                            ...userObjTemlate,
                                            "userName": value.participant[i].name,
                                            "userId": value.participant[i].id,
                                        };
                                        userObjs.push(inviteUserObj);
                                    }
                                    // debugger
                                    // addMembersApi(userObjs);
                                    invitemembersApi(userObjs);

                                    this.props.navigation.goBack();
                                });
                            });
                        }
                    });
                } else {
                    alert("Please make sure you are connect to Internet");
                }
            });
        }
        
    }

    render() {
        return (
            <View style={styles.container}>
                <InfoForm onRef={(ref)=>(this.createEventForm = ref)}>
                    
                </InfoForm>
                <TouchableHighlight 
                    onPress={()=>{this.createEvent()}}
                    title = "press"
                    style = {styles.createEventBtn}
                    underlayColor = "#ffffff"
                >
                    <Text style={styles.createEventBtnTxt}>
                        Create
                    </Text>
                </TouchableHighlight>
            </View>
        );
    }
}

export default connect((state) => {
    return {
        ...state.userInfo,
        fbUserId: state.userInfo.fbUserId,
        fbUserName: state.userInfo.fbUserName,
    };
})(CreateEventScreen);