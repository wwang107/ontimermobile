import React from "react";
import { Text, View, Modal, StyleSheet, TouchableHighlight, Keyboard} from "react-native";
import InfoForm from "./InfoForm"
import {Header, Icon, Item, Input} from "native-base";
import {addMember, confirmOrJoin} from "../../api/events.js";
import {connect} from "react-redux";
import {setFireTime} from "../../api/pushNotification.js";
const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        // alignItems: "center",
    },
    joinEventBtn:{
        // flex:1, 
        height: 44,
        width: 300,
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 10,
        borderColor : "transparent",
        justifyContent: "center",
        backgroundColor: "#00e700",
        alignSelf: "center",
    },
    joinEventBtnTxt:{
        fontSize:20, 
    },
    header: {
        // alignItems: 'stretch' ,
        backgroundColor: 'transparent',
        // width: 500,
        borderBottomWidth: 0,
        alignSelf: 'stretch'
    }
});

export class JoinEventScreen extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            modalVisible: true,
            searchText: "",
        };
    }
    
    setModalVisible(visible) {
        this.setState({modalVisible: visible});
    }

    componentDidMount() {
        this.joinEventForm.changePermission("join");
        const searchIdFromNav = this.props.navigation.getParam('searchId', null);
        if(this.props.searchId){
            this.handleSearch(this.props.searchId);
        }else if(searchIdFromNav){
            this.handleSearch(searchIdFromNav.toString());
        }

    }
    
    componentWillUnmount() {
        this.setModalVisible(false);
    }
    handleSearch(e) {
        if(typeof(e)==="string"){
            this.setState({searchText: e}, this.joinEventForm.searchEventById(Number(e)));
        }else{
            this.setState({searchText: e.nativeEvent.text});
            this.joinEventForm.searchEventById(Number(e.nativeEvent.text));
        }
        
    }

    handleClear() {
        this.setState({searchText: ""});
    }

    joinEvent(){
        var value = this.joinEventForm.getValue();
        console.log(value);
        if(value){
            let userObj = {
                ...value,
                userName: this.props.fbUserName,
                userId: this.props.fbUserId,
                confirm: 1,
                // dateTime: new Date(value.eventDateTime).getTime() / 1000,
            };

            confirmOrJoin(userObj).then((res)=>{
                if(res==="joined"){
                    alert("Already joined event");
                }else if(res.id){
                    setFireTime(this.props.fbUserId, value.eventId, value.dateTime, value.alarmTime, "member", {...value});
                    alert("Join event successful");
                }else{
                    alert("Join event falied, please check internet state");
                }
                this.props.navigation.goBack();
            });
        }
    }

    render() {
        return (
            
            <View style={styles.container}>

                <Header searchBar rounded style={styles.header}>
                    <Item style={{backgroundColor: 'white'}}>
                        <Icon name='ios-search'
                            onPress={()=>{Keyboard.dismiss()}}/>
                        <Input autoFocus placeholder='Search'
                            keyboardType='numeric'
                            defaultValue={this.state.searchText}
                            onEndEditing={(e)=>{this.handleSearch(e);}}/>
                        <Icon name='close'
                            onPress={()=>{this.handleClear();}} />
                    </Item>
                </Header>

                <InfoForm onRef={(ref)=>(this.joinEventForm = ref)}>
                </InfoForm>
                <TouchableHighlight 
                    onPress={()=>{this.joinEvent()}}
                    title = "press"
                    style = {styles.joinEventBtn}
                    underlayColor = "#ffffff"
                >
                    <Text style={styles.joinEventBtnTxt}>
                        Join
                    </Text>
                </TouchableHighlight>
            </View>
            
        );
    }

    // render() {
    //     return (
    //         <Modal
    //             animationType="slide"
    //             transparent={false}
    //             visible={this.state.modalVisible}
    //             onRequestClose={() => {
    //                 this.setModalVisible(false);
    //             }}>
    //             <View style={styles.container}>
    //                 <InfoForm onRef={(ref)=>(this.joinEventForm = ref)}>
                        
    //                 </InfoForm>
    //                 <TouchableHighlight 
    //                     onPress={()=>{this.joinEventForm.searchEventById(15);}}
    //                     title = "press"
    //                     style = {styles.joinEventBtn}
    //                     underlayColor = "#ffffff"
    //                 >
    //                     <Text style={styles.joinEventBtnTxt}>
    //                         Add
    //                     </Text>
    //                 </TouchableHighlight>
    //             </View>
    //         </Modal>
    //     );
    // }
}
export default connect((state) => {
    return {
        fbUserId: state.userInfo.fbUserId,
        fbUserName: state.userInfo.fbUserName,
    };
})(JoinEventScreen);