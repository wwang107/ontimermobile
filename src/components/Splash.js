import React, { Component } from 'react';
import { NavigationActions } from 'react-navigation';
import {isLoggedIn} from '../api/fb.js';
import {View, ImageBackground, BackHandler} from 'react-native';
import {connect} from 'react-redux';
import {setFbUserInfo, setFbInfo} from '../states/user-actions';
import {getUserInfo as getInfoFromFbApi} from "../api/fb.js"
import {getCurrentRouteName} from "../utility/navUtility.js";

export class Splash extends Component {

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.androidHardwareBackHandler);
        this.checkLoggedIn();
    }
    
    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.androidHardwareBackHandler);
    }

    //air bnb version
    androidHardwareBackHandler = () => {
        const mainScreens = ['Add Event', 'Notification', 'Account'];
        let {routes} = this.props;
        let currentRouteName = getCurrentRouteName(routes);
        console.log(currentRouteName);
        if(currentRouteName==="Login" || currentRouteName==="Home" ||  currentRouteName==="Splash"){
            return false;
            
        }else if(mainScreens.includes(currentRouteName)){
            this.props.dispatch(NavigationActions.navigate({routeName: "Home"}));
            return true

        }else{
            this.props.dispatch(NavigationActions.back());
            return true;
        }
    }

    checkLoggedIn() {
        isLoggedIn().then((res)=>{
            if(res){

                // this.props.dispatch(setFbUserInfo());
                getInfoFromFbApi().then((info)=>{
                    this.props.dispatch(setFbInfo(info));
                    this.props.navigation.navigate("HomeNav");
                }).catch((error)=>{
                    throw new Error(error);
                });
            } else {
                this.props.navigation.navigate("Login");
            }

            // console.log(res);
            // const routeName = res ? "HomeNav" : "Login";
            // this.props.navigation.navigate(routeName);

        }).catch((error)=>{
            this.props.navigation.navigate("Login");
        });
    }

    render() {
        return(
        <ImageBackground source={require('../img/money_time_2.jpg')} resizeMode='cover' style={{
            width: 500,
            height: 680,
            paddingRight: 70}}>
        </ImageBackground>
        );
    }
}

export default connect((state) => {
    return {
        ...state.userInfo,
        ...state.nav,
    };
})(Splash);