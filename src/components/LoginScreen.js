import React, {Component} from 'react';
import {
	AppRegistry,
	StyleSheet,
	Text,
	View,
	TouchableHighlight,
	ImageBackground
} from 'react-native';
import {LoginButton, AccessToken, GraphRequestManager, GraphRequest} from 'react-native-fbsdk';
import {fbLoginManager, getToken, getFriendList, isLoggedIn, getUserInfo as getInfoFromFbApi} from '../api/fb.js';
import * as Animatable from 'react-native-animatable';
import {Image} from 'react-native';
import {setFbUserInfo, setFbInfo} from '../states/user-actions';
import {connect} from 'react-redux';

const userBaseUrl = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com";

export class testapp extends Component {

	initUser(token) {
        fetch("https://graph.facebook.com/v2.5/me?fields=email,name,friends&access_token=" + token)
        .then((response) => {return response.json()})
        .then(json => {
            console.log('json.id', json.id);
            let url = `${userBaseUrl}/api/userinfo?userid=${encodeURIComponent(json.id)}`;
            console.log('url', url);
            return fetch(url)
            .then(res => {
                if (res.status !== 200) throw new Error('200 error!');
                return res.json();
            }).then(data => {
                if (data.length == 0) {
                    // add user
                    var addUserURL = `${userBaseUrl}/api/adduser`;
                    return fetch(addUserURL, {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                            "userid" : json.id,
                            "username" : json.name,
                            "userphonenumber" : "0966666666"
                        })
                    }).catch(error => {
                        console.log(error);
                    });     
                } else {
                    console.log('data', data);
                }
            }).catch(error => {
                console.log(error);
            })
        })
    };

  
  //uncomment this to check login in login page
  componentWillMount(){
     isLoggedIn().then((result)=>{
       if(result)
         this.props.navigation.navigate('HomeNav');
     });
   }

	render() {
    	return (
      		<ImageBackground source={require('../img/money_time_2.jpg')} resizeMode='cover' style={styles.bgImg}>
      			<View style={styles.container}>
        			<View style={styles.logo}>
          				<Animatable.Image easing="easeIn" animation='bounceInDown' style={styles.logoImg} source={require('../img/logo.png')} />
          				<Animatable.Text easing="easeIn" animation='bounceInRight'style={styles.logoText}>OnTimer</Animatable.Text>
        			</View>
        
        			<Animatable.View easing="easeIn" animation='bounceInLeft' style={styles.login}>
          				<LoginButton
          					readPermissions={["public_profile"]}
          					onLoginFinished={
            					(error, result) => {
              						if (error) {
                						alert("Login failed with error: " + result.error);
             						} else if (result.isCancelled) {
                						alert("Login was cancelled");
              						} else {
										AccessToken.getCurrentAccessToken().then((data) => {
											//alert(data.accessToken.toString());
											const {accessToken} = data;
											this.initUser(accessToken);
										});
                            alert("Login was successful with permissions: " + result.grantedPermissions);
                            getInfoFromFbApi().then((info)=>{
                                this.props.dispatch(setFbInfo(info));
                                this.props.navigation.navigate('HomeNav');
                            }).catch((error)=>{
                                throw new Error(error);
                            });
              						}
            					}
          					}
          					onLogoutFinished={() => alert("User logged out")}/>
          				<TouchableHighlight
            				onPress={()=>{this.props.navigation.navigate('HomeNav');}}>
            				<Text>Go to home page</Text>
          				</TouchableHighlight>
        			</Animatable.View>

        			<View styles={styles.passBtn}>
          				<TouchableHighlight
            				onPress={()=>{this.props.navigation.navigate('HomeNav');}}>
            				<Text>Go to home page</Text>
          				</TouchableHighlight>
        			</View>   
      			</View>
    		</ImageBackground>
	);
  }
}

const styles = StyleSheet.create({
	container: {
    	flex: 1,
    	justifyContent: 'center',
    	alignItems: 'center',
    	//backgroundColor: '#F5FCFF',
  	},
  	bgImg: {
    	width: 500,
    	height: 680,
    	paddingRight: 70
  	},
  	logo: {
    	flex: 5,
    	justifyContent: 'center',
    	alignItems: 'center',
 	},
  	logoImg:{
    	width: 200, 
    	height: 200,
  	},
  	logoText:{
    	fontSize : 50,
    	//color: 'white',
    	backgroundColor: 'transparent',
    	fontFamily : 'GloriaHallelujah',
  	},
  	login: {
    	flex:2,
    	alignItems: 'center',
  	},
  	passBtn:{
    	flex:1,
  	},
  	welcome: {
    	fontSize: 20,
    	textAlign: 'center',
    	margin: 10,
  	},
  	instructions: {
    	textAlign: 'center',
    	color: '#333333',
    	marginBottom: 5,
  	},
});

export default connect((state) => {
    return {
        ...state.userInfo,
    };
})(testapp);
// AppRegistry.registerComponent('testapp', () => testapp);
