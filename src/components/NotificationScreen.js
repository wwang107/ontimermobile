import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Text, View, StyleSheet, TouchableOpacity, Image, ScrollView } from 'react-native';
import Modal from 'react-native-modal'
import { Container, Header, Title, Button, Icon, Tabs, Tab, Right, Left, Body } from 'native-base';
import InvitationsTab from './NotificationScreenSubpage/InvitationsTab';
import CashFlowTab from './NotificationScreenSubpage/CashFlowTab';
import UserInfoModal from './NotificationScreenSubpage/UserInfoModal';

const userEvents = [];
const eventInfo = [];
const eventsURLs = [];
const cashflowEventsURLs = [];

const styles = StyleSheet.create({
	header: {
		// backgroundColor: 'rgba(255, 255, 255, 0.7)'
	},

	headerText: {
		paddingHorizontal: 25,
		// color: "black"
	},

	container: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},

	horizontal: {
		flexDirection: 'row',
		justifyContent: 'space-around',
		padding: 10
	}
});

class NotificationScreen extends Component {

	userURL = `http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api/events?userid=${this.props.fbUserId}`;
	eventBaseURL = 'http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api/eventinfo?eventid=';
	cashflowByUserURL = `http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api/logByUser?userid=${this.props.fbUserId}`;
	cashflowByEventURL = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api/logByEvent?eventid=";

    constructor(props) {
		super(props);
		this.state = {
			eventInfoList: [],
			cashflowByUser: [],
			cashflowByEvent: []
		}

		this.fetchEvents = this.fetchEvents.bind(this);
		this.fetchCashFlow = this.fetchCashFlow.bind(this);
	}

	fetchEvents() {
		/* clear the list */
		userEvents = []
		eventInfo = []
		eventsURLs = []
		this.setState({ eventInfoList: [] });

		// GET
		return fetch(this.userURL)
		.then((res) => res.json())
		.then((data) => {
			if (data) {
				data.map(info => {
					eventsURLs.push(this.eventBaseURL + info.eventid);
					userEvents.push(info);
				});
				return Promise.all(eventsURLs.map((eventURL) => {
					return fetch(eventURL)
						.then(res => res.json())
						.then((info) => {
							eventInfo.push(info[0]);
						});
				}));
			}
		})
		.then(() => {
			userEvents.map((info, id) => {
				this.state.eventInfoList.push({
					// from userEvents
					userid: this.props.fbUserId,
					username: this.props.fbUserName,
					eventid: userEvents[id].eventid,
					deposit: userEvents[id].deposit,
					arrivetime: userEvents[id].arrivetime,
					late: userEvents[id].late,
					confirm: userEvents[id].confirm,
					memberStatus: userEvents[id].status,
					// from eventInfo
					eventname: eventInfo[id].eventname,
					datetime: eventInfo[id].datetime,
					mindeposit: eventInfo[id].mindeposit,
					maxdeposit: eventInfo[id].maxdeposit,
					address: eventInfo[id].address,
					about: eventInfo[id].about,
					hoster: eventInfo[id].hoster,  // this is hoster's id
					hostername: eventInfo[id].hostername,
					currency: eventInfo[id].currency,
					ts: eventInfo[id].ts,
					eventStatus: eventInfo[id].status
				});
				this.setState({ eventInfoList: this.state.eventInfoList });  // re-render the screen
			});
		})
		.catch((error) => {
			console.log("Fetching events error!");
			console.log(error);
		});
	}

	fetchCashFlow() {
		cashflowEventsURLs = []
		this.setState({ cashflowByUser: [], cashflowByEvent: [] });

		return fetch(this.cashflowByUserURL)
		.then((res) => res.json())
		.then((data) => {
			if (data) {
				data.map(info => {
					cashflowEventsURLs.push(this.cashflowByEventURL + info.eventid);
					this.state.cashflowByUser.push(info);
				});
				return Promise.all(cashflowEventsURLs.map((cashflowEventsURL) => {
					return fetch(cashflowEventsURL)
						.then(res => res.json())
						.then((info) => {
							this.state.cashflowByEvent.push(info);
						});
				}));
			}
		})
		.then(() => {
			this.setState({ cashflowByUser: this.state.cashflowByUser });  // re-render the screen
			this.setState({ cashflowByEvent: this.state.cashflowByEvent });  // re-render the screen
		})
		.catch((error) => {
			console.log("Fetching cash flow lists error!");
			console.log(error);
		});
	}

	componentWillMount() {
		console.log("Notification screen is mount!");
		this.fetchEvents();
		this.fetchCashFlow();
	}
	
    render() {
		
        return (
			<Container>
				<Header hasTabs style={styles.header}>
					<Body>
						<Title style={styles.headerText}>Notifications</Title>
					</Body>
					<Right>
            			<Button transparent><Icon name="search" /></Button>
            			<Button transparent><Icon name="more" /></Button>
          			</Right>
				</Header>
				<Tabs style={styles.header} locked={true}>
					<Tab heading="Invitations">
						<InvitationsTab eventInfoList={this.state.eventInfoList} onRefresh={this.fetchEvents} navigation={this.props.navigation}/>
					</Tab>
					<Tab heading="Cash Flow">
						<CashFlowTab cashflowUserlist={this.state.cashflowByUser} cashflowEventlist={this.state.cashflowByEvent} onRefresh={this.fetchCashFlow}/>
					</Tab>
				</Tabs>
			</Container>
        );
    }
}

export default connect((state, ownProps) => ({
	fbUserId: state.userInfo.fbUserId,
	fbUserName: state.userInfo.fbUserName,
}))(NotificationScreen);