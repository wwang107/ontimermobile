import {signInEvent as signInEventFromApi} from '../api/events'
import {timeConverter} from '../api/utilities'
import React from 'react'; 
import { Dimensions, StyleSheet, View, Button, Alert, Image, ActivityIndicator} from 'react-native';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import { Container,Content, Body, List, ListItem, Text, Left, Right} from 'native-base'; 
import { Col,Grid } from "react-native-easy-grid";
import { connect } from 'react-redux';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import {getUserPict} from '../api/fb'
import { getEvent, getDeviceLoction } from '../states/event-actions'

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.03;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

const CustomHeader = ({ title, subtitle }) => (
    <View style={styles.header}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.subtitle}>{subtitle}</Text>
    </View>
);

class EventDetail extends React.Component {

    static navigationOptions = ({ navigation}) => ({
        headerTitle: <CustomHeader title={timeConverter(navigation.state.params.datetime, 1)} subtitle={navigation.state.params.eventTitle} />,
    });

    constructor(props) {
        super(props);
        this.state = {
            signIn: this.props.events.user[this.props.navigation.state.params.index].status,
            late: this.props.events.user[this.props.navigation.state.params.index].late
        }
    
    }

    signIn(eventid, datetime, fbUserId, deviceLocation, eventLocation){

        dLat = deviceLocation.lat;
        dLon = deviceLocation.long;
        eLat = eventLocation.latitude;
        eLon = eventLocation.longitude;
        if ((datetime - (new Date().getTime()) / 1000) > 1800) {
            console.log('!!!!!!!!!');
            console.log((datetime - (new Date().getTime()) / 1000) );
            Alert.alert('Too early to sign in', 'Please sign in 30 mins before event starting');
            return;
        }

        if (_measure(dLat, dLon, eLat, eLon) > 1000) {
            Alert.alert('Too far from event location', 'Please sign in only within the range of 1 km from the event location');
            return;
        }
        console.log('!!!!!!!!!');
        console.log(datetime / 1000 - (new Date().getTime()) / 1000);

        signInEventFromApi(eventid, datetime, fbUserId)        
            .then(res => {
                    return res.json();
            })
            .then(status=>{

                status.late ? this.setState({
                    signIn:true,
                    late: true
                }) : this.setState({
                    signIn: true,
                    late: false
                })
            })
            .catch(err => console.log('Sending request for event sign in failed: ', err));
}

    render() {
        const { about, address, eventid, maxdeposit, hostername, mindeposit,datetime,totalmoney, longitude, latitude} = this.props.events.events[this.props.navigation.state.params.index]
        const eventRegion = { latitude: latitude, longitude: longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA};
        const userRegion = { latitude: this.props.deviceLocation.lat, longitude: this.props.deviceLocation.long, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
        
        // console.log('this is event region: ', eventRegion)
        // console.log('this is user region: ', userRegion);
        
        // console.log(this.props.events.events[this.props.navigation.state.params.index]);
        return (
            
        <Container>
            <Content>
                <List>
                    <ListItem itemDivider>
                        <Left>
                            <SimpleLineIcons name="map" size={20} />
                            <Body>
                                <Text>{address}</Text>
                            </Body>
                        </Left>
                    </ListItem>
                    <ListItem >
                        <Left></Left>
                        <MapView
                            provider={null}
                            style={styles.map}
                            scrollEnabled={true}
                            zoomEnabled={true}
                            pitchEnabled={true}
                            rotateEnabled={false}
                            initialRegion={eventRegion}
                        >
                        
                        <MapView.Circle
                            center={{latitude, longitude}}
                            radius={1000}
                            strokeWidth={1}
                            strokeColor={'#1a66ff'}
                            fillColor={'rgba(0,191,255,0.2)'}
                        />
                            <Marker
                                title="Event location"
                                description="Get into the blue circle to sign in!"
                                coordinate={eventRegion}
                            />

                            <Marker
                                title= 'You'
                                coordinate={userRegion}
                            >
                                <View>
                                <Image
                                            style={{ width: 35, height: 35, borderRadius: 35 / 2, borderWidth: 2, borderColor:'#008b8b'}}
                                    source={{uri:getUserPict(this.props.fbUserId)}}
                                />
                                </View>
                            </Marker>

                        </MapView>
                        <Right></Right>
                    </ListItem>
                    <ListItem itemDivider>
                            <Grid>
                                <Col>
                                <SimpleLineIcons name="wallet" size={25} />
                                </Col>
                                <Col>
                                    <Body>
                                        <Text style={styles.listTitle}>Max Deposit</Text>
                                        <Text>{maxdeposit}</Text>
                                    </Body>
                                </Col>         
                                
                                <Col>
                                <Body>
                                    <Text style={styles.listTitle}>Min Deposit</Text>
                                    <Text>{mindeposit}</Text>
                                </Body>
                                </Col>
                                <Col>
                                <Body>
                                    <Text style={styles.listTitle}>Total. Deposit</Text>
                                    <Text>{totalmoney}</Text>
                                </Body>
                                </Col>
                            </Grid>
                    </ListItem>
                        <ListItem itemDivider>
                            <Grid>
                                <Col>
                                    <SimpleLineIcons name="info" size={25} />
                                </Col>
                                <Col>
                                    <Body>
                                        <Text style={styles.listTitle}>About</Text>
                                        <Text>{about}</Text>
                                    </Body>
                                </Col>
                                <Col>
                                    <Body>
                                        <Text style={styles.listTitle}>Event ID</Text>
                                        <Text>{eventid}</Text>
                                    </Body>
                                </Col>
                                <Col>
                                </Col>
                            </Grid>
                        </ListItem>
                    <ListItem itemDivider>
                        <Grid>
                            <Col>
                                <SimpleLineIcons name="user" size={25} />
                            </Col>
                            <Col>
                                <Body>
                                    <Text style={styles.listTitle}>Host</Text>
                                    <Text>{hostername}</Text>
                                </Body>
                            </Col>

                            <Col>
                            </Col>
                            <Col>
                            </Col>
                        </Grid>
                    </ListItem>

                    <ListItem itemDivider>
                        <Grid>
                                <Col>
                                    {(this.state.signIn) ? (
                                            <Button
                                                onPress={()=>{}}
                                                title={this.state.late ? 'LATE' : 'ON TIME'}
                                                color={this.state.late ? '#8b0000' : '#006400'}
                                    />
                                        ) : (
                                            <Button
                                                onPress={() => this.signIn(eventid, datetime, this.props.fbUserId, this.props.deviceLocation,{latitude,longitude})}
                                                title='SIGN IN'
                                                color="#6495ed"
                                            />
                                        )}
                                    
                                </Col>
                        </Grid>
                    </ListItem>

                </List>
            </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    scrollview: {
        alignItems: 'center',
        paddingVertical: 40,
    },
    map: {
        width: 400,
        height: 225,
    },
    header: {
        backgroundColor: '#fff',
        flex: 1,
        alignSelf: 'stretch',
    },
    listTitle:{
        fontWeight:'bold'
    },
    title: {
        fontSize: 16,
        color: 'gray',
        fontWeight: 'bold',
    },
    subtitle: {
        fontSize: 25,
        color: 'black',
        fontWeight: 'bold',
    }
});

function _measure(lat1, lon1, lat2, lon2) {  // generally used geo measurement function
    var R = 6378.137; // Radius of earth in KM
    var dLat = lat2 * Math.PI / 180 - lat1 * Math.PI / 180;
    var dLon = lon2 * Math.PI / 180 - lon1 * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d * 1000; // meters
}

export default connect((state) => {
    return {
        fbUserId:state.userInfo.fbUserId,
        ...state.eventReducer,
        ...state.nav
    };
  })(EventDetail);