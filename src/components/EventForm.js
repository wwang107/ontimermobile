import React from "react";
import { Text, View, Modal, TouchableHighlight, Button, StyleSheet, Image} from "react-native";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import {getGeoByAddress} from "../api/googleMap.js";
const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "center",
        alignItems: "center",
    },
    createEventBtn:{
        // flex:1, 
        height: 44,
        width: 300,
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 10,
        borderColor : "transparent",
        justifyContent: "center",
        backgroundColor: "#7ae4ff"

    },
    createEventBtnTxt:{
        fontSize:20, 
    },
    joinEventBtn:{
        // flex:1, 
        marginTop : 10,
        height: 44,
        width: 300,
        alignItems: "center",
        borderWidth: 1,
        borderRadius: 10,
        borderColor : "transparent",
        justifyContent: "center",
        backgroundColor: "#7ae4ff"

    },
    joinEventBtnTxt:{
        fontSize:20, 
    },
});

export default class EventForm extends React.Component {
    


    render() {
        return (
            <View style={styles.container}>
                <TouchableHighlight 
                    onPress={()=>{this.props.navigation.navigate("CreateEventScreen")}}
                    title = "press"
                    style = {styles.createEventBtn}
                    underlayColor = "#ffffff"
                >
                    <Text style={styles.createEventBtnTxt}>
                        Create Event!
                    </Text>
                </TouchableHighlight>
                <TouchableHighlight 
                    onPress={()=>{this.props.navigation.navigate("JoinEventScreen")}}
                    title = "press"
                    style = {styles.joinEventBtn}
                    underlayColor = "#ffffff"
                >
                    <Text style={styles.joinEventBtnTxt}>
                        Join Event!
                    </Text>
                </TouchableHighlight>

            </View>
        );
    }
    // state = {
    //     modalVisible: false,
    // };

    // setModalVisible(visible) {
    //     this.setState({modalVisible: visible});
    // }
    // render() {
    //     return (
    //       <View style={{marginTop: 22}}>
    //         <Modal
    //           animationType="slide"
    //           transparent={false}
    //           visible={this.state.modalVisible}
    //           onRequestClose={() => {
    //             alert("Modal has been closed.");
    //           }}>
    //           <View style={{marginTop: 22}}>
    //             <View>
    //               <Text>Hello World!</Text>

    //               <TouchableHighlight
    //                 onPress={() => {
    //                   this.setModalVisible(!this.state.modalVisible);
    //                 }}>
    //                 <Text>Hide Modal</Text>
    //               </TouchableHighlight>
    //             </View>
    //           </View>
    //         </Modal>

    //         <TouchableHighlight
    //           onPress={() => {
    //             this.setModalVisible(true);
    //           }}>
    //           <Text>Show Modal</Text>
    //         </TouchableHighlight>
    //       </View>
    //     );
    // }
}


// <GooglePlacesAutocomplete
//   placeholder='Search'
//   minLength={1} // minimum length of text to search
//   autoFocus={false}
//   returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
//   listViewDisplayed='auto'    // true/false/undefined
//   fetchDetails={true}
//   renderDescription={row => row.description} // custom description render
//   onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
//     console.log(data, details);
//     getGeoByAddress(data.description).then((res)=>{
//         console.log(res);
//     });
//   }}
  
//   getDefaultValue={() => ''}
  
//   query={{
//     // available options: https://developers.google.com/places/web-service/autocomplete
//     key: 'AIzaSyCH_RiXhxchoQA8Lr0UHJFU7BOnsjWhGis',
//     language: 'en', // language of the results
//     types: 'establishment' // default: 'geocode'
//   }}
  
//   styles={{
//     textInputContainer: {
//       width: '100%'
//     },
//     description: {
//       fontWeight: 'bold'
//     },
//     predefinedPlacesDescription: {
//       color: '#1faadb'
//     }
//   }}
  
//   currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
//   currentLocationLabel="Current location"
//   nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
//   GoogleReverseGeocodingQuery={{
//     // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
//   }}
//   GooglePlacesSearchQuery={{
//     // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
//     rankby: 'distance',
//     types: 'food'
//   }}

//   filterReverseGeocodingByTypes={['locality', , "political", 'administrative_area_level_5']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
//   predefinedPlaces={[homePlace, workPlace]}

//   debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
//   // renderLeftButton={()  => <Image source={require(s'path/custom/left-icon')} />}
//   renderRightButton={() => <Text>Custom text after the input</Text>}
// />
