import React from 'react';
import {View, FlatList, ActivityIndicator, StyleSheet} from 'react-native';
import {Container, Fab, Icon, Button} from 'native-base';
import {connect} from 'react-redux';
import EventItem from './EventItem'
import { getEvent, getDeviceLoction} from '../states/event-actions'

class HomeScreen extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        headerTitle: `${navigation.state.routeName}`
        });

    constructor(props) {
        super(props);
        this.state = {
            fabActive: false
        };
        this.onRefresh = this.onRefresh.bind(this);
    }
    
    onRefresh(){
        console.log('refreshing')
        geoOption = { enableHighAccuracy: false, timeout: 30000, maximumAge: 10000};
        this.props.dispatch(getDeviceLoction(geoOption));
        this.props.dispatch(getEvent(this.props.fbUserId));
    }
    componentDidMount() {
        geoOption = { enableHighAccuracy: false, timeout: 30000, maximumAge: 10000};
        this.props.dispatch(getDeviceLoction(geoOption));
        this.props.dispatch(getEvent(this.props.fbUserId));
    }
    
    handleFabClose(){

    }

    render() {
        console.log(this.props);
        const {loadingEvent, loadingDeviceLocation} = this.props;
        return (
            <Container>
                {(loadingEvent || loadingDeviceLocation) ? (
                    <View style={[styles.container, styles.horizontal]}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                ):(
                        <FlatList
                            onRefresh={()=>this.onRefresh()}
                            refreshing={loadingEvent}
                            data={this.props.events.user}
                            renderItem={({ item, index }) => {
                                if (item.arrivetime === null) 
                                    return (
                                    <EventItem
                                        eventTitle={item.eventname} host={item.hostname}
                                        deposit={item.deposit} eta={item.eta} index={index} />
                                    );
                                else
                                    return (null);
                        }}
                            keyExtractor={(item) => item.eventid.toString()}
                        />   
                )}
            <Fab
                active={this.state.fabActive}
                direction="up"
                containerStyle={{ }}
                style={{ backgroundColor: '#5067FF' }}
                position="bottomRight"
                onPress={() => this.setState({ fabActive: !this.state.fabActive })}>
                <Icon name="add" />
                <Button 
                    style={{ backgroundColor: '#34A34F' }}
                    onPress = {()=>{this.props.navigation.navigate("CreateEventScreen");}}
                >
                  <Icon name="create" />
                </Button>
                <Button 
                    style={{ backgroundColor: '#3B5998' }}
                    onPress = {()=>{this.props.navigation.navigate("JoinEventScreen");}}
                >
                  <Icon name="person-add" />
                </Button>
          </Fab>
            </Container>

        );
    }

}

export default connect((state) => {
    return {
        ...state.eventReducer,
        fbUserId: state.userInfo.fbUserId,
        fbUserName: state.userInfo.fbUserName,
    };
})(HomeScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
    },
    fabMask: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.5)'
    },
    fabContainer: {
        marginLeft: 10
    },
    fab: {
        backgroundColor: '#31b0d5',
    },
})
