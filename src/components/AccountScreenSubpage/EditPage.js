import React from 'react';
import { View, StyleSheet, Image, BackHandler, Keyboard } from 'react-native';
import {
    Container, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Separator, Button, Item, Input,
} from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';


import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { NavigationActions } from 'react-navigation';

import {setIsEdited} from '../../states/user-actions.js';

const iconProps= {
  size: 25,
  color: '#A0A0A0',
};

export class EditPage extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
    });

    static propTypes = {
    };

    constructor(props){
        super(props);
        this.state = {
            wordLimit : 0,
            reduxAction : null,
            wordCount : 0,
            inputText : '',
        };

    }

    componentWillMount(){
        const {wordLimit, originText, reduxAction, keyboardType} = this.props.navigation.state.params;
        this.setState({
            wordLimit: wordLimit,
            wordCount: originText.length,
            inputText: originText,
            reduxAction: reduxAction,
            keyboardType: keyboardType,
        });
    }


    render() {
        const {wordLimit, wordCount, inputText, keyboardType} = this.state;

        return (
            <View style={styles.editPage}>
                <View style={styles.topBox} />
                <View style={styles.bottomBox}>
                    <View style={styles.wordLimitBox}>
                        <Text style={styles.wordLimitHint}>{wordCount}/{wordLimit}</Text>
                    </View>
                    <View style={styles.inputBox}>
                        <Content>
                            <Item regular>
                                <Input onChangeText={this.handleTextChange} keyboardType={keyboardType} maxLength={wordLimit} value={inputText}/>
                                <MaterialIcons style={{marginRight: 10}} name="cancel" {...iconProps} onPress={this.handleResetText} />
                            </Item>
                        </Content>
                    </View>
                    <View style={styles.saveButtonBox}>
                        <Button block primary onPress={this.handleButtonSave}>
                            <Text>Save</Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }

    handleButtonSave = () => {
        // call the given action to update edited content
        this.props.dispatch(this.state.reduxAction(this.state.inputText));
        this.props.dispatch(setIsEdited());

        // navigate GoBack
        this.props.dispatch(NavigationActions.back());

        // Hide that keyboard!
        Keyboard.dismiss();
    }

    handleTextChange = (inputText) => {
        this.setState({
            inputText: inputText,
            wordCount: inputText.length,
        })
    }

    handleResetText = () => {
        this.setState({
            inputText: '',
            wordCount: 0,
        });
    }
}

const styles = StyleSheet.create({
    editPage : {
        flex: 1,
        justifyContent: 'center',
        padding: 20,


    },

    topBox :{
        flex: 1,
    },
    bottomBox :{
        flex: 10,
    },

    wordLimitBox: {
        justifyContent: 'flex-end',
        flexDirection: 'row',
    },
    wordLimitHint:{
        fontSize: 20,
        color: 'gray',
    },
    inputBox: {
        height: 60,
        justifyContent: 'center',
    },
    saveButtonBox: {
        justifyContent: 'center',
    },
});


export default connect((state, ownProps) => ({
    ...state.nav,
}))(EditPage);
