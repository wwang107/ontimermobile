import React from 'react';
import { Text, View, Header} from 'react-native';

export default class AttendEventTab extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>List Attended Events Here!</Text>
            </View>
        );
    }
}
