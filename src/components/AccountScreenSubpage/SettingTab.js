import React from 'react';
import { View } from 'react-native';
import { Container, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Separator, Button } from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import { NavigationActions } from 'react-navigation';

import Entypo from 'react-native-vector-icons/Entypo';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import {updateUserName} from "../../states/user-actions.js";

// develop purpose
import SignInPage from '../SignInSystem/SignInPage';
import {fbLoggedOut} from '../../api/fb.js'

const iconProps= {
  size: 25,
  color: '#C0C0C0',
};


export class SettingTab extends React.Component {

    static propTypes = {
    };

    constructor(props) {
        super(props);

        this.handleNavigate = this.handleNavigate.bind(this);
    }

    handleNavigate(route, title){
      this.props.dispatch(NavigationActions.navigate({routeName: route,
                                                    params: {title: title}}));
      // this.props.navigation.navigate(route, {title: 'Edit Profile'});
    }

    render() {
        const isNotiOnStr = this.props.allNotificationFlag? 'On' : 'Off';
        return (
          <Container>
            <Content>
              <List>
                <Separator bordered/>

                <ListItem icon button={true} onPress={()=>this.handleNavigate('EditProfileNav', 'Edit Profile') } first>
                  <Left>
                    <SimpleLineIcons name="user" size={25}/>
                  </Left>
                  <Body>
                    <Text >Profile</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>
                {/*
                <ListItem icon button={true} onPress={()=>this.handleNavigate('CreateUserNav', 'Create User')} >
                  <Left>
                    <SimpleLineIcons name="layers" size={25}/>
                  </Left>
                  <Body>
                    <Text >Create User(debug)</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>
                */}
                <ListItem icon button={true} onPress={()=>this.handleNavigate('PaymentPageNav', 'Your Payment')}>
                  <Left>
                    <SimpleLineIcons name="credit-card" size={25}/>
                  </Left>
                  <Body>
                    <Text >Payment</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>

                <ListItem icon button={true} onPress={()=>this.handleNavigate('EditNotificationNav', 'Edit Notification') }>
                  <Left>
                    <SimpleLineIcons name="volume-2" size={25} />
                  </Left>
                  <Body>
                    <Text >Notifications</Text>
                  </Body>
                  <Right>
                    <Text>{isNotiOnStr}</Text>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>
                {/*
                <ListItem icon button={true}>
                  <Left>
                    <SimpleLineIcons name="globe" size={25}/>
                  </Left>
                  <Body>
                    <Text >Language</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>

                <Separator bordered/>
                <ListItem icon button={true} >
                  <Left>
                    <SimpleLineIcons name="bubble" size={25}/>
                  </Left>
                  <Body>
                    <Text >FeedBack</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>
                */}

                <ListItem icon button={true} onPress={()=>this.handleNavigate('AboutPageNav', 'About OnTimer')}>
                  <Left>
                    <SimpleLineIcons name="question" size={25}/>
                  </Left>
                  <Body>
                    <Text >About OnTimer</Text>
                  </Body>
                  <Right>
                    <Entypo name="chevron-right" {...iconProps} />
                  </Right>
                </ListItem>

                <Separator bordered/>
                <Button full light onPress={()=>{
                  console.log(this.props);
                  fbLoggedOut();
                  this.props.dispatch(NavigationActions.navigate({routeName: "Login"}));
                }}>
                  <Text style={{fontWeight: 'bold'}}>Sign out</Text>
                </Button>

              </List>
            </Content>
          </Container>
        );
    }
}
export default connect((state, ownProps) => ({
  ...state.userInfo,
  ...state.flagControler,
  ...state.nav,
}))(SettingTab);
