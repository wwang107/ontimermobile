import React from 'react';
import { View, StyleSheet, Image, BackHandler, Keyboard } from 'react-native';
import {
    Container, Content, Text, Icon, List, ListItem, Separator, Left, Body, Right,
} from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';


import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { NavigationActions } from 'react-navigation';

import {setIsEdited} from '../../states/user-actions.js';

const iconProps= {
  size: 25,
  color: '#C0C0C0',
};

const currentVersion = "0.0.1"

export class AboutPage extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
    });

    static propTypes = {
    };

    constructor(props){
        super(props);
        this.state = {
            wordLimit : 2000,
            wordCount : 0,
            inputText : '',
        };

    }


    render() {
        const {wordLimit, wordCount, inputText, keyboardType} = this.state;

        return (
            <View style={styles.wholePage}>
                <Container>
                    <List>
                        <Separator bordered/>

                        <ListItem >
                            <Body>
                                <Text style={styles.listText}>Current version</Text>
                            </Body>
                            <Right>
                                <Text style={{color:'#A0A0A0', fontWeight:'bold'}}>{currentVersion}</Text>
                            </Right>
                        </ListItem>

                        <Separator bordered/>
                        <ListItem  button={true} onPress={()=>this.handleNavigate('PrivatePolicyPageNav', 'Private Policy') } >
                            <Body>
                                <Text style={styles.listText}>Private Policy</Text>
                            </Body>
                            <Right>
                                <Entypo name="chevron-right" {...iconProps} />
                            </Right>
                        </ListItem>

                        <ListItem button={true} onPress={()=>this.handleNavigate('TermsOfServicePageNav', 'Terms of Service') } >
                            <Body>
                                <Text style={styles.listText}>Terms of Service</Text>
                            </Body>
                            <Right>
                                <Entypo name="chevron-right" {...iconProps} />
                            </Right>
                        </ListItem>
                    </List>
                </Container>
            </View>
        );
    }

    handleNavigate(route, title){
      this.props.dispatch(NavigationActions.navigate({routeName: route,
                                                    params: {title: title}}));
      // this.props.navigation.navigate(route, {title: 'Edit Profile'});
    }
}

const styles = StyleSheet.create({
    wholePage : {
        flex: 1,
        backgroundColor: 'white',
    },
    listText : {
        fontSize: 18,
    }
});


export default connect((state, ownProps) => ({
    ...state.nav,
}))(AboutPage);
