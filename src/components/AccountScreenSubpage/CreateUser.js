import React from 'react';
import { View, BackHandler, Picker } from 'react-native';
import { Container, Header, Content, Form, Item, Input, Label, Body, Title, Text, Button } from 'native-base';

import Feather from 'react-native-vector-icons/Feather';

import {connect} from 'react-redux';

import { NavigationActions } from 'react-navigation';

import {createUser} from '../../states/user-actions.js'

import {postSignin} from '../../api/signIn.js'

export class CreateUser extends React.Component {

  static navigationOptions = ({ navigation }) => ({
      title: `${navigation.state.params.title}`,
  });

  constructor(props){
    super(props);
    this.state = {
      edited: false,
      inputUserId: 0,
      inputUserName: '',
      inputPhoneNumber: '',
    };

    this.handleButtonCreate = this.handleButtonCreate.bind(this);
    this.handleButtonBack = this.handleButtonBack.bind(this);
  }

  render() {
    return (
      <Container>
        <Content>
          <Form>
            <Item stackedLabel>
              <Label>User ID</Label>
              <Input onChangeText={(inputUserId)=>this.setState({inputUserId})} />
            </Item>
            <Item stackedLabel last>
              <Label>User Name</Label>
              <Input onChangeText={(inputUserName)=>this.setState({inputUserName})}/>
            </Item>
            <Item stackedLabel last>
              <Label>User Phone Number</Label>
              <Input onChangeText={(inputPhoneNumber)=>this.setState({inputPhoneNumber})}/>
            </Item>
          </Form>

          <Text>{this.state.inputUserId}</Text>
          <Text>{this.state.inputUserName}</Text>
          <Text>{this.state.inputPhoneNumber}</Text>
          <Button rounded primary onPress={this.handleButtonCreate}>
            <Text>Create!</Text>
          </Button>
          <Button rounded primary onPress={this.handleButtonBack}>
            <Text>BACK!</Text>
          </Button>
          <Button rounded primary onPress={this.handleArrive}>
            <Text>Arrive!!</Text>
          </Button>
          <Button rounded primary onPress={this.handleLate}>
            <Text>LATE!!</Text>
          </Button>

          <Picker
            selectedValue={this.state.inputUserId}
            style={{ height: 50, width: 100 }}
            onValueChange={(itemValue, itemIndex) => this.setState({inputUserId: itemValue})}>
            <Picker.Item label="Java" value="java" />
            <Picker.Item label="JavaScript" value="js" />
          </Picker>
        </Content>
      </Container>
    );

  }

  handleButtonCreate(){
    userInfo={
      userId: this.state.inputUserId,
      userPhoneNumber: this.state.inputPhoneNumber,
      userName: this.state.inputUserName,
    };
    this.props.dispatch(createUser(userInfo));
  }

  handleButtonBack(){
    this.props.dispatch(NavigationActions.back());
  }

  handleArrive = () => {

      const currentTimestamp = Math.floor(Date.now() / 1000);
      const userInfo={
          eventId : 4,
          userId : "4228282",
          currentTimestamp : currentTimestamp,
          isLate : false,
      };

      postSignin(userInfo);
  }

  handleLate = () => {
      const currentTimestamp = Math.floor(Date.now() / 1000);
      const userInfo={
          eventId : 4,
          userId : "4228282",
          currentTimestamp : currentTimestamp,
          isLate : true,
      };

      postSignin(userInfo);
  }
}

export default connect((state, ownProps) => ({

}))(CreateUser);
