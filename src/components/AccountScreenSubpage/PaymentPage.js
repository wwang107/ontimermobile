import React from 'react';
import { View, StyleSheet, Image, BackHandler, Keyboard } from 'react-native';
import {
    Container, Content, Text, Icon, List, ListItem, Separator, Left, Body, Right, Button
} from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';


import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

import { NavigationActions } from 'react-navigation';

import { CreditCardInput, LiteCreditCardInput } from "react-native-credit-card-input";

import {getUserPayment, setUserPayment} from '../../states/user-actions.js'

const iconProps= {
  size: 25,
  color: '#C0C0C0',
};


export class PaymentPage extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
    });

    static propTypes = {
        userPaymentList: PropTypes.array,
    };

    constructor(props){
        super(props);
        this.state = {
            newPayment: null,
            isAddCreditCard: false,
        };

    }

    componentDidMount(){
        this.props.dispatch(getUserPayment());
    }


    render() {
        const {userPaymentList} = this.props;
        const {isAddCreditCard} = this.state;
        console.log(userPaymentList);

        return (
            <View style={styles.wholePage}>
                <Container>
                    <List>
                        <Separator bordered/>
                        {userPaymentList?userPaymentList.map((payment)=>(
                            <ListItem icon>
                                <Left>
                                  <SimpleLineIcons name="credit-card" size={25}/>
                                </Left>
                                <Body>
                                    <Text>**** **** **** {payment.values.number.split(" ")[3]}</Text>
                                </Body>
                            </ListItem>
                        )):<View/>}

                        {!isAddCreditCard ?
                            <ListItem icon button={true} onPress={this.handleAddCreditCard}>
                                <Left>
                                  <MaterialIcons name="add-circle-outline" size={25}/>
                                </Left>
                                <Body>
                                    <Text style={{color:'#A0A0A0'}}>Add Credit Card</Text>
                                </Body>
                            </ListItem>
                        :<View/>}
                        {isAddCreditCard ?
                            <View>
                                <ListItem>
                                    <LiteCreditCardInput onChange={this._onChangePayment} />
                                </ListItem>
                                <View style={{marginLeft: 10, marginRight:10}}>
                                    <Button block primary onPress={this.handleButtonAdd} >
                                        <Text>ADD</Text>
                                    </Button>
                                </View>
                            </View>
                        :<View/>}
                    </List>
                </Container>
            </View>
        );
    }

    handleAddCreditCard = () => {
        this.setState({isAddCreditCard: true});
    }

    _onChangePayment = (form) => {
        this.setState({newPayment: form});
    }

    handleButtonAdd = () => {
        const {newPayment} = this.state;
        var {userPaymentList} = this.props;
        userPaymentList = userPaymentList ? userPaymentList : []; //prevent null
        if(newPayment.valid){
            this.props.dispatch(setUserPayment([...userPaymentList, newPayment]));
        }

        this.setState({isAddCreditCard: false, newPayment: null});
    }
}

const styles = StyleSheet.create({
    wholePage : {
        flex: 1,
        backgroundColor: 'white',
    },
    listText : {
        fontSize: 18,
    },

});


export default connect((state, ownProps) => ({
    ...state.nav,
    userPaymentList: state.userInfo.userPaymentList,
}))(PaymentPage);
