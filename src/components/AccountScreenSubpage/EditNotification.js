import React from 'react';
import { View, StyleSheet, Image, BackHandler } from 'react-native';
import { Container, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Separator, Button } from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";

import { NavigationActions } from 'react-navigation';

import {toggleAllNotification, toggleUpcomingNotification, toggleNewInviteNotification} from '../../states/flag-actions.js';


const iconProps= {
  size: 25,
  color: '#C0C0C0',
};

const additionalWordsProps= {
  size: 40,
  color: '#C0C0C0',
};

export class EditNotification extends React.Component {
    static propTypes = {
        allNotificationFlag: PropTypes.bool,
        newInviteNotificationFlag: PropTypes.bool,
        upcomingNotificationFlag: PropTypes.bool,
    };

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
    });

    constructor(props){
        super(props);
        this.state = {
            notificationFlag: false,
        };

    }

    render() {
        const {allNotificationFlag, newInviteNotificationFlag, upcomingNotificationFlag} = this.props;

        return (
            <View style={styles.editNotificationPage}>

                <View style={styles.notificationListBox}>
                    <Container>
                        <Content>
                            <List>
                                <ListItem icon first>
                                  <Body>
                                    <Text size={30}>Notifications</Text>
                                  </Body>
                                  <Right>
                                    <Switch value={allNotificationFlag} onValueChange={()=>this.props.dispatch(toggleAllNotification())} />
                                  </Right>
                                </ListItem>
                                <Separator style={styles.additionalInfos}>
                                    <Text {...additionalWordsProps} >Force closing the app may cause notifications to arrive late or not be delivered.</Text>
                                </Separator>
                            </List>
                            {allNotificationFlag ? (
                                <List>
                                    <ListItem icon first>
                                      <Body>
                                        <Text size={30}>New Invite</Text>
                                      </Body>
                                      <Right>
                                        <Switch value={newInviteNotificationFlag} onValueChange={()=>this.props.dispatch(toggleNewInviteNotification())} />
                                      </Right>
                                    </ListItem>

                                    <ListItem icon first>
                                      <Body>
                                        <Text size={30}>Upcoming Event</Text>
                                      </Body>
                                      <Right>
                                        <Switch value={upcomingNotificationFlag} onValueChange={()=>this.props.dispatch(toggleUpcomingNotification())} />
                                      </Right>
                                    </ListItem>


                                </List>
                            ):<View />}
                        </Content>
                    </Container>
                </View>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    editNotificationPage : {
        flex: 1,
        justifyContent: 'center'
    },


    notificationListBox : {
        flex: 1,
        backgroundColor: 'white',
    },


    additionalInfos: {
        height: 80,

    }
});


export default connect((state, ownProps) => ({
    allNotificationFlag: state.flagControler.allNotificationFlag,
    newInviteNotificationFlag: state.flagControler.newInviteNotificationFlag,
    upcomingNotificationFlag: state.flagControler.upcomingNotificationFlag,
}))(EditNotification);
