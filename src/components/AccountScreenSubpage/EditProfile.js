import React from 'react';
import { View, StyleSheet, Image, BackHandler, Picker } from 'react-native';
import { Container, Content, List, ListItem, Text, Icon, Left, Body, Right, Switch, Separator, Button } from 'native-base';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import Ionicons from "react-native-vector-icons/Ionicons";

import { NavigationActions } from 'react-navigation';

import {createUser, getUserInfo,
    updateUserName, updateUserPhoneNumber, updateUserEmail, updateUserGender, updateUserHomeLocation, updateUserPrepareTime,
    calculatePrepareTime,
    setIsEdited
} from '../../states/user-actions.js';
import {getUserPict} from "../../api/fb.js";

const uri = "https://cdn1.iconfinder.com/data/icons/unique-round-blue/93/user-256.png";

const iconProps= {
  size: 25,
  color: '#C0C0C0',
};

const userNameWordLimit = 20;
const userPhoneNumberLimit = 10;
const userEmailWordLimit = 256;
const userHomeLocationWordLimit = 256;
export class EditProfile extends React.Component {

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.title}`,
    });

    constructor(props){
        super(props);
        this.state = {
            gender: 'unselected'
        };

    }

    render() {
        console.log(this.props);
        let {userName, userPhoneNumber, userPrepareTime, userGender, userEmail, userHomeLocation, userRecommandTime, userId} = this.props;
        let displayUserName = userName.length>20 ? (userName.substring(0, 19) + "...") : userName;
        userPrepareTime =Number(userPrepareTime);
        // console.log(userPrepareTime);

        const recommandPrepareTimeMin = Math.round(userRecommandTime/60);

        const isRecommandTime = !([5*60, 10*60, 15*60, 30*60, 60*60, 65*60].indexOf(userRecommandTime) > -1);

        return (
            <View style={styles.editProfilePage}>
                <View style={styles.accountPhotoBox}>
                    <Image cover style={styles.accountPicture} source={{uri:getUserPict(userId, "large")}}/>
                </View>

                <View style={styles.profileListBox}>
                    <Container>
                        <Content>
                            <List>
                                <ListItem icon first button={true} onPress={() => this.handleNavigateToEditPage(
                                    'Nickname', userNameWordLimit, userName, updateUserName)} >
                                  <Left>
                                    <SimpleLineIcons name="user" size={25}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Nickname</Text>
                                  </Body>
                                  <Right>
                                    <Text size={30}>{displayUserName}</Text>
                                    <Entypo name="chevron-right" {...iconProps} />
                                  </Right>
                                </ListItem>

                                <ListItem icon first button={true} onPress={() => this.handleNavigateToEditPage(
                                    'Phone Number', userPhoneNumberLimit, userPhoneNumber, updateUserPhoneNumber, "numeric")} >
                                  <Left>
                                    <Entypo name="phone" size={25}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Phone number</Text>
                                  </Body>
                                  <Right>
                                    <Text size={30}>{userPhoneNumber}</Text>
                                    <Entypo name="chevron-right" {...iconProps} />
                                  </Right>
                                </ListItem>

                                <ListItem icon first button={true} onPress={() => this.handleNavigateToEditPage(
                                    'Home Location', userHomeLocationWordLimit, userHomeLocation, updateUserHomeLocation)} >
                                  <Left>
                                    <Feather name="map-pin" size={25}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Home Location</Text>
                                  </Body>
                                  <Right>
                                    <Entypo name="chevron-right" {...iconProps} />
                                  </Right>
                                </ListItem>

                                <ListItem icon first button={true} onPress={() => this.handleNavigateToEditPage(
                                    'Email Address', userEmailWordLimit, userEmail, updateUserEmail, "email-address")} >
                                  <Left>
                                    <Entypo name="email" size={25}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Email Address</Text>
                                  </Body>
                                  <Right>
                                    <Entypo name="chevron-right" {...iconProps} />
                                  </Right>
                                </ListItem>

                                <ListItem icon first>
                                  <Left>
                                    <FontAwesome name="venus-mars" size={25}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Gender</Text>
                                  </Body>
                                  <Right>
                                      <Picker
                                        mode="dropdown"
                                        selectedValue={userGender}
                                        style={styles.dropdownList}
                                        onValueChange={(itemValue, itemIndex)=>this.props.dispatch(updateUserGender(itemValue))}
                                      >
                                        <Picker.Item label="Male" value="male" />
                                        <Picker.Item label="Female" value="female" />
                                        <Picker.Item label="Others" value="others" />
                                      </Picker>
                                  </Right>
                                </ListItem>

                                <ListItem icon first>
                                  <Left>
                                    <Ionicons name="md-time" size={30}/>
                                  </Left>
                                  <Body>
                                    <Text size={30}>Prepare Time</Text>
                                  </Body>
                                  <Right>
                                      <Picker
                                        mode="dropdown"
                                        selectedValue={userPrepareTime}
                                        style={styles.dropdownList}
                                        onValueChange={(itemValue, itemIndex)=>{
                                            this.props.dispatch(setIsEdited())
                                            this.props.dispatch(updateUserPrepareTime(itemValue))}}
                                      >
                                        <Picker.Item label=" 5 min" value={5*60} />
                                        <Picker.Item label="10 min" value={10*60} />
                                        <Picker.Item label="15 min" value={15*60} />
                                        <Picker.Item label="30 min" value={30*60} />
                                        <Picker.Item label="60 min" value={60*60} />
                                        <Picker.Item label="60 min up" value={65*60} />
                                        <Picker.Item label={`${recommandPrepareTimeMin} min (Recommand)`} value={userRecommandTime}/>
                                      </Picker>
                                  </Right>
                                </ListItem>
                                {/*TODO: delete this button */}
                                <Button onPress={()=>this.props.dispatch(calculatePrepareTime(2,"103081045",  userRecommandTime))}>
                                    <Text>calculate preparetime</Text>
                                </Button>
                            </List>
                        </Content>
                    </Container>
                </View>
            </View>
        );
    }

    handleNavigateToEditPage = (title, wordLimit, originText, reduxAction, keyboardType) => {
        this.props.dispatch(NavigationActions.navigate({routeName: 'EditPageNav',
                                                      params: {
                                                          title: title,
                                                          wordLimit: wordLimit,
                                                          originText: originText,
                                                          reduxAction: reduxAction,
                                                          keyboardType: keyboardType,
                                                      }}));
    }
}

const styles = StyleSheet.create({
    editProfilePage : {
        flex: 1,
        justifyContent: 'center'
    },

    accountPhotoBox : {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    profileListBox : {
        flex: 4,
        backgroundColor: 'white',
    },


    accountPicture : {
        width: 100,
        height: 100,
        borderRadius:100,
        borderWidth: 2,
        borderColor:'rgba(255,255,255,0.5)',
    },

    dropdownList : {
        height: 50,
        width: 100,
        color: '#A0A0A0',
        justifyContent: 'center'
    },

    previewBlockLimit: {
        height: 50,
        width: 100,
    }
});


export default connect((state, ownProps) => ({
    ...state.userInfo,
}))(EditProfile);
