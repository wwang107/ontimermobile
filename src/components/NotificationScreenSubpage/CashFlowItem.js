import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Animated, Easing, View, StyleSheet, Image, TouchableOpacity, ScrollView } from 'react-native';
import { ListItem, Thumbnail, Body, Right, Text } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';
import { toggleCashFlowListItem } from '../../states/notifications-actions'
import { timeConverter } from '../../api/utilities'
import { getUserPict } from '../../api/fb';
import UserInfoModal from '../NotificationScreenSubpage/UserInfoModal';

const styles = StyleSheet.create({
    besideArrow: {
        textAlign: 'right',
        alignSelf: 'stretch',
        width: 110,
        fontSize: 15,
        fontWeight: '400'
    },
    
    gain: {
        color: '#2ECC40'
    },

    lose: {
        color: '#FF4136'
    },

    breakeven: {
        color: '#000080'
    },

    collapsedItem: {
        flex: 1,
        paddingVertical: 10,
        borderColor: '#c0c0c0',
        borderBottomWidth: 0.3
    },

    ulItem: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        paddingLeft: 35,
        paddingRight: 85,
        paddingVertical: 3,
        width: 410
    },

    ulImage: {
        width: 35,
        height: 35,
        borderRadius: 50,
        marginHorizontal: 4
    },

    flexContainer: {
        flex: 5,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-between'
    },

    userNameScrollView: {
        paddingHorizontal: 5,
        width: 250
    },

    eventNameScrollView: {
        marginLeft: 12,
        width: 170
    },

    eventName: {
        marginLeft: 0
    }
});

class CashFlowItem extends React.Component {

	spinValue = new Animated.Value(0)

    static propTypes = {
        item: PropTypes.object,
        id: PropTypes.number,
        collapsed: PropTypes.bool,
        cashflowEvent: PropTypes.array
    }

    constructor(props) {
        super(props);

        this.state = {
            toValue: 1,
            isModalVisible: false,
            modalUserPict: null,
            modalUserName: null,
            modalUserPhone: null,
            modalUserEmail: null
		};

        this._toggleCashFlowListItem = this._toggleCashFlowListItem.bind(this);
        this._toggleModal = this._toggleModal.bind(this);
        this._onBackdropPress = this._onBackdropPress.bind(this);
	}

    componentWillReceiveProps() {
        this.setState({ toValue: this.state.toValue == 0 ? 1 : 0 });

		Animated.timing(
			this.spinValue,
			{
				toValue: this.state.toValue,
				duration: 150,
                easing: Easing.linear,
                useNativeDriver: true
			}
		).start()
    }

	_toggleCashFlowListItem() {
		Animated.timing(
			this.spinValue,
			{
				toValue: this.state.toValue,
				duration: 150,
                easing: Easing.linear,
                useNativeDriver: true
			}
		).start()

        this.props.dispatch(toggleCashFlowListItem(this.props.id));
        this.setState({ toValue: this.state.toValue == 0 ? 1 : 0 });
    };

	_toggleModal(id, name, phone, email) {
        this.setState({
            isModalVisible: !this.state.isModalVisible,
            modalUserPict: getUserPict(id, 'large'),
            modalUserID: id,
            modalUserName: name,
            modalUserPhone: phone,
            modalUserEmail: email
        });
    }

    _onBackdropPress() {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    render() {
        const { item, cashflowEvent } = this.props;

		const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '90deg']
        })

        let cashflowStyle = (item.cashflow >= 0 ? (item.cashflow == 0 ? styles.breakeven : styles.gain) : styles.lose);
        
        let cashflow = (
            item.cashflow >= 0 ? (
                item.cashflow == 0 ? 'NT$0' : '+ NT$' + item.cashflow
            ) : (
                '- NT$' + Math.abs(item.cashflow)
            ));

        let listitem = null;

        if (cashflowEvent) {
            listitem = cashflowEvent.map((eventItem, i) => (
                <View style={styles.ulItem} key={i}>
                    <Text>{'\u2022  '}</Text>
                    <TouchableOpacity onPress={() => this._toggleModal(eventItem.userid, eventItem.username, this.props.userPhoneNumber, '')} activeOpacity={0.6}>
                        <Image style={styles.ulImage} source={{ uri: getUserPict(eventItem.userid, 'large') }} />
                    </TouchableOpacity>
                    <ScrollView style={styles.userNameScrollView} horizontal={true} showsHorizontalScrollIndicator={false}>
                        <Text>{eventItem.username}</Text>
                    </ScrollView>
                    <Text>{' :   '}</Text>
                    <Text style={cashflowStyle}>{cashflow}</Text>
                </View>
            ))
        }

        
        return (
            <View>
                <ListItem onPress={this._toggleCashFlowListItem}>
                    <TouchableOpacity onPress={() => this._toggleModal(item.hoster, item.hostername, this.props.userPhoneNumber, '')} activeOpacity={0.6}>
                        <Thumbnail source={{ uri: getUserPict(item.hoster, 'large') }} />
                    </TouchableOpacity>
                    <View style={styles.flexContainer}>
                        <Body>
                            <ScrollView style={styles.eventNameScrollView} horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity>
                                    <Text style={styles.eventName}>{item.eventname}</Text>
                                </TouchableOpacity>
                            </ScrollView>
                            <Text note style={styles.listText}>{timeConverter(item.datetime, 0)}</Text>
                        </Body>
                        <View style={{flexDirection: "row", alignSelf: 'center'}}>
                            {/* <Text note style={[styles.besideArrow, {item.cashflow > 0 ?}]}>{item.cashflow}</Text> */}
                            <Text style={[styles.besideArrow, cashflowStyle ]}>
                                {cashflow}
                            </Text>
                            <Animated.View style={{transform: [{rotate: spin}], alignSelf: 'center', marginHorizontal: 3 }}>
                                <Feather name="chevron-right" size={20}/>
                            </Animated.View>
                        </View>
                    </View>
                </ListItem>
                <Collapsible duration={150} collapsed={this.props.collapsed} align='center'>
                    <View style={styles.collapsedItem}>{listitem}</View>
                </Collapsible>
                <UserInfoModal
                    isModalVisible={this.state.isModalVisible}
                    onBackdropPress={this._onBackdropPress}
                    avatarURL={this.state.modalUserPict}
                    UserName={this.state.modalUserName}
                    UserPhone={this.state.modalUserPhone}
                    UserEmail={this.state.modalUserEmail}
                />
            </View>
        );
    }
}

export default connect((state, ownProps) => ({
    userPhoneNumber: state.userInfo.userPhoneNumber,
    collapsed: !state.cashflowItem.collapsed[ownProps.id]
}))(CashFlowItem);