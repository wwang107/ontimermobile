import React, { Component } from 'react';
import { Text, View, StyleSheet, Image, ScrollView } from 'react-native';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import { getUserPict } from '../../api/fb';

const styles = StyleSheet.create({
    modalContent: {
		flex: 0,
		width: 320,
		height: 450,
		backgroundColor: 'rgba(0, 0, 0, 0)',
		// padding: 22,
		justifyContent: "center",
		alignItems: "center",
		alignSelf: 'center',
		// borderRadius: 15,
		// borderWidth: 10,
		// borderColor: "rgb(118, 23, 196)",
	},

    bgImageContainer: {
        position: 'absolute',
    },

	bgImage: {
		flex: 1,
		// alignSelf: 'stretch',
        borderRadius: 15,
        width: 320,
        height: 450,
		resizeMode: 'cover', // or 'stretch'
	},
    
    avatarContainer: {
        flex: 1,
        marginTop: 55,
        borderColor: 'rgba(255, 255, 255, 1)',
		borderWidth: 4,
        borderRadius: 8,
        width: 125,
        height: 125
    },

	avatar: {
		flex: 1,
		// borderColor: 'rgba(255, 255, 255, 0.5)',
		// borderWidth: 4,
		borderRadius: 0.3,
		// width: '50%',
		// height: '50%'
		// width: 300,
        // height: 300,
        // alignSelf: 'stretch'
        width: null,
        height: null,
        resizeMode: 'contain',
        // resizeMode: 'cover'
	},
    
    userNameContainer: {
        // flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 30,
        width: 240,
    },

    userName: {
        fontSize: 22,
        color: 'rgba(0, 0, 0, 0.8)',
        alignSelf: 'center',
    },

	modalTextContainer: {
        flex: 1,
        paddingBottom: 10,
        marginBottom: 50,
		backgroundColor: 'transparent',
		justifyContent: 'flex-start',
		width: 240,
	},

	modalText: {
		fontSize: 18,
	},

	ulItem: {
		flex: 0,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        paddingVertical: 7
        // width: 240
	},
});

export default class UserInfoModal extends Component {

    imageURL = 'https://png.pngtree.com/thumb_back/fw800/back_pic/04/59/32/96586a82a124de4.jpg';
    // imageURL = require('../../img/userInfoBG.jpg');

    static propTypes = {
        isModalVisible: PropTypes.bool,
        onBackdropPress: PropTypes.func,
        avatarURL: PropTypes.string,
        UserName: PropTypes.string,
		UserPhone: PropTypes.string,
		UserEmail: PropTypes.string
	}
	
	constructor(props) {
		super(props);
	};

    render() {
        const {UserName, UserPhone, UserEmail} = this.props;

        return (
            <View style={styles.container}>
            <Modal 
                isVisible={this.props.isModalVisible} 
                onBackdropPress={this.props.onBackdropPress}
                useNativeDriver={true}
            >
                <View style={styles.modalContent}>
                    <View style={styles.bgImageContainer}>
                        <Image style={styles.bgImage} source={{uri: this.imageURL}} />
                        {/* <Image style={styles.bgImage} source={require('../../img/userInfoBG.jpg')} /> */}
                    </View>
                    <View style={styles.avatarContainer}>
                        <Image style={styles.avatar} source={{uri: this.props.avatarURL}} />
                    </View>
                    <View style={styles.userNameContainer}>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                            <Text style={styles.userName}>{this.props.UserName}</Text>
                        </ScrollView>
                    </View>
                    <View style={styles.modalTextContainer}>
                        <View style={styles.ulItem}>
                            <Text style={styles.modalText}>{'\u2022  '}Phone : </Text>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <Text style={styles.modalText}>{this.props.UserPhone}</Text>
                            </ScrollView>
                        </View>
                        <View style={styles.ulItem}>
                            <Text style={styles.modalText}>{'\u2022  '}Email : </Text>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <Text style={styles.modalText}>{this.props.UserEmail}</Text>
                            </ScrollView>
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
        );
    };
}