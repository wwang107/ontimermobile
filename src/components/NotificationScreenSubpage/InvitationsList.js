import React from 'react';
import PropTypes from 'prop-types';
import InvitationsItem from './InvitationsItem.js';
import UnconfirmedItem from './UnconfirmedItem.js';
import { View, StyleSheet } from 'react-native';
import { List, Separator, Text } from 'native-base';

const styles = StyleSheet.create({
    listSeperatorText: {
        fontSize: 12
    }
});

export default class InvitationsList extends React.Component {

    static propTypes = {
        historylist: PropTypes.array,
        unconfirmedlist: PropTypes.array
    }

	constructor(props) {
		super(props);
	}

    render() {
        const { historylist, unconfirmedlist } = this.props;

        return (
            <View>
                {
                    !!unconfirmedlist.length &&  // '!!' is necessary
                    <View>
                        <Separator bordered>
                            <Text style={styles.listSeperatorText}>Unconfirmed</Text>
                        </Separator>
                        <List>
                            {
                                unconfirmedlist.map((item, i) => (
                                    <UnconfirmedItem item={item} key={i} id={i} navigation={this.props.navigation}/>
                                ))
                            }
                        </List>
                    </View>
                }
                {
                    !!historylist.length &&
                    <View>
                        <Separator bordered>
                            <Text style={styles.listSeperatorText}>History</Text>
                        </Separator>
                        <List>
                            {
                                historylist.map((item, i) => {
                                    if (item.memberStatus || item.eventStatus) {
                                        return (
                                            <InvitationsItem item={item} key={i} id={i}/>
                                        );
                                    }
                                })
                            }
                        </List>
                    </View>
                }
            </View>
        );
    }
}