import React from 'react';
import { ScrollView, RefreshControl } from 'react-native';
import { Container } from 'native-base';
import PropTypes from 'prop-types';
import CashFlowList from './CashFlowList.js';
	
export default class CashFlowTab extends React.Component {

    static propTypes = {
        cashflowUserlist: PropTypes.array,
		cashflowEventlist: PropTypes.array,
		onRefresh: PropTypes.func
	}
	
	constructor(props) {
		super(props);
		this.state = {
			refreshing: false
		};
		this._onRefresh.bind(this);
	};

	_onRefresh() {
		this.setState({ refreshing: true });
		this.props.onRefresh().then(() => {
			this.setState({ refreshing: false });
		});
	};

	render() {
		const { cashflowUserlist, cashflowEventlist, eventInfoList } = this.props;

		return (
			<Container>
				<ScrollView
					directionalLockEnabled={true}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh.bind(this)} />
					}>
					<CashFlowList cashflowUserlist={cashflowUserlist} cashflowEventlist={cashflowEventlist}/>
				</ScrollView>
			</Container>
		);
	}
}