import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Animated, Easing, View, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, ScrollView } from 'react-native';
import { ListItem, Thumbnail, Body, Right, Text } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';
import Collapsible from 'react-native-collapsible';
import { toggleUnconfirmedListItem } from '../../states/notifications-actions'
import { timeConverter } from '../../api/utilities'
import { getUserPict } from '../../api/fb';
import UserInfoModal from '../NotificationScreenSubpage/UserInfoModal';

const styles = StyleSheet.create({
    besideArrow: {
        // textAlign: 'right',
        alignSelf: 'center',
        width: 90
    },

    collapsedItem: {
        paddingVertical: 10,
        borderColor: '#c0c0c0',
        borderBottomWidth: 0.3
    },

    ulItem: {
        flex: 0,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        paddingHorizontal:35,
        paddingVertical: 3,
        width: 310
    },

    flexContainer: {
        flex: 5,
        flexDirection: 'row',
        flexWrap: 'nowrap',
        justifyContent: 'space-between'
    },

    eventNameScrollView: {
        marginLeft: 12,
        width: 100
    },

    eventName: {
        marginLeft: 0
    },

    confirmButton: {
        borderRadius: 12,
        backgroundColor: 'rgba(73, 155, 13, 0.8)',
        justifyContent: 'center',
        alignItems: 'center',
        height: 30
    },

    confirmButtonText: {
        color: 'rgba(255, 255, 255, 1)',
        fontSize: 16,
        paddingHorizontal: 2
    }
});

class UnconfirmedItem extends React.Component {

	spinValue = new Animated.Value(0)

    static propTypes = {
        item: PropTypes.object,
        id: PropTypes.number,
        collapsed: PropTypes.bool
    }

    constructor(props) {
        super(props);

        this.state = {
            toValue: 1,
            isModalVisible: false
		};

        this._toggleUnconfirmedListItem = this._toggleUnconfirmedListItem.bind(this);
        this._toggleModal = this._toggleModal.bind(this);
        this._onBackdropPress = this._onBackdropPress.bind(this);
	}

    componentWillReceiveProps() {
        this.setState({ toValue: this.state.toValue == 0 ? 1 : 0 });

		Animated.timing(
			this.spinValue,
			{
				toValue: this.state.toValue,
				duration: 150,
                easing: Easing.linear,
                useNativeDriver: true
			}
		).start()
    }

	_toggleUnconfirmedListItem() {
		Animated.timing(
			this.spinValue,
			{
				toValue: this.state.toValue,
				duration: 150,
                easing: Easing.linear,
                useNativeDriver: true
			}
		).start()

        this.props.dispatch(toggleUnconfirmedListItem(this.props.id));
        this.setState({ toValue: this.state.toValue == 0 ? 1 : 0 });
    };
    
	_toggleModal() {
        this.setState({
            isModalVisible: !this.state.isModalVisible});
    }

    _onBackdropPress() {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    }

    render() {
        const { item } = this.props;

		const spin = this.spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '90deg']
        })

        return (
            <View>
                <ListItem onPress={this._toggleUnconfirmedListItem}>
                    <TouchableOpacity onPress={this._toggleModal} activeOpacity={0.6}>
                        <Thumbnail source={{ uri: getUserPict(item.hoster, 'large') }} />
                    </TouchableOpacity>
                    <View style={styles.flexContainer}>
                        <Body>
                            <ScrollView style={styles.eventNameScrollView} horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity>
                                    <Text style={styles.eventName}>{item.eventname}</Text>
                                </TouchableOpacity>
                            </ScrollView>
                            <Text note style={styles.listText}>{timeConverter(item.datetime, 0)}</Text>
                        </Body>
                        <TouchableOpacity
                            onPress = {()=>{this.props.navigation.navigate("JoinEventScreen", {searchId: item.eventid});}}
                            style={[styles.confirmButton, styles.besideArrow]}
                            activeOpacity={0.6}>
                            <Text style={styles.confirmButtonText}>Confirm</Text>
                        </TouchableOpacity>
                        <View style={{flexDirection: "row", alignSelf: 'center'}}>
                            {/* <Text note style={styles.besideArrow}>NT${item.deposit}</Text> */}
                            <Animated.View style={{transform: [{rotate: spin}], alignSelf: 'center', marginHorizontal: 3 }}>
                                <Feather name="chevron-right" size={20}/>
                            </Animated.View>
                        </View>
                    </View>
                </ListItem>
                <Collapsible duration={150} collapsed={this.props.collapsed} align="center">
                    <View style={styles.collapsedItem}>
                        <View style={styles.ulItem}>
                            <Text>{'\u2022  '}Event name : </Text><Text>{item.eventname}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Hoster : </Text><Text>{item.hostername}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Deopsit range : </Text><Text>NT${item.mindeposit} ~ NT${item.maxdeposit}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Date time : </Text><Text>{timeConverter(item.datetime, 1)}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Arrived time : </Text><Text>{timeConverter(item.arrivetime, 1)}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Address : </Text><Text>{item.address}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}About : </Text><Text>{item.about}</Text>
                        </View><View style={styles.ulItem}>
                            <Text>{'\u2022  '}Event created time : </Text><Text>{timeConverter(item.ts, 1)}</Text>
                        </View>
                    </View>
                </Collapsible>
                <UserInfoModal
                    isModalVisible={this.state.isModalVisible}
                    onBackdropPress={this._onBackdropPress}
                    avatarURL={getUserPict(item.hoster, 'large')}
                    UserName={item.hostername}
                    UserPhone={this.props.userPhoneNumber}
                    UserEmail={''}
                />
            </View>
        );
    }
}

export default connect((state, ownProps) => ({
    userPhoneNumber: state.userInfo.userPhoneNumber,
    collapsed: !state.unconfirmedItem.collapsed[ownProps.id]
}))(UnconfirmedItem);