import React from 'react';
import PropTypes from 'prop-types';
import CashFlowItem from './CashFlowItem.js';
import { List } from 'native-base';

export default class CashFlowList extends React.Component {

    static propTypes = {
        cashflowUserlist: PropTypes.array,
        cashflowEventlist: PropTypes.array  // this is a 2D array
    }

    render() {
        const { cashflowUserlist, cashflowEventlist, eventInfoList } = this.props;
        return (
            <List>
                {
                    cashflowUserlist.map((item, i) => (
                        <CashFlowItem item={item} key={i} id={i} cashflowEvent={cashflowEventlist[i]}/>  // so this is an 1D array
                    ))
                }
            </List>
        );
    }
}