import React from 'react';
import { ScrollView, RefreshControl } from 'react-native';
import { Container } from 'native-base';
import PropTypes from 'prop-types';
import InvitationsList from './InvitationsList.js';

const historylist = [];
const unconfirmedlist = [];

export default class InvitationsTab extends React.Component {

    static propTypes = {
		eventInfoList: PropTypes.array,
		onRefresh: PropTypes.func
	}

	constructor(props) {
		super(props);
		this.state = {
			refreshing: false
		};
		this._onRefresh.bind(this)
	}

	_onRefresh() {
		this.setState({ refreshing: true });
		this.props.onRefresh().then(() => {
			this.setState({ refreshing: false });
		});
	}

	render() {
		const { eventInfoList } = this.props;
		historylist = [];
		unconfirmedlist = [];

		eventInfoList.map((eventInfo) => {
			if (eventInfo.confirm) {
				historylist.push(eventInfo);
			} else {
				unconfirmedlist.push(eventInfo);
			}
		});

		return (
			<Container>
				<ScrollView
					directionalLockEnabled={true}
					refreshControl={
						<RefreshControl
							refreshing={this.state.refreshing}
							onRefresh={this._onRefresh.bind(this)} />
					}>
					<InvitationsList historylist={historylist} unconfirmedlist={unconfirmedlist} navigation={this.props.navigation}/>
				</ScrollView>
			</Container>
		);
	}
}