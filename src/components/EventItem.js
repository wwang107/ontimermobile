
import { travelTimeEst } from '../api/google'
import React from 'react';
import {StyleSheet, 
        View,
        Dimensions
      } from 'react-native';
import MapView, { Marker, ProviderPropType } from 'react-native-maps';
import {connect} from 'react-redux';
import { Card, CardItem, Text, Button, Icon, Left, Body, Right} from 'native-base';
import { NavigationActions } from 'react-navigation'
import { timeConverter} from '../api/utilities'

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.004757;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

class EventItem extends React.Component {

    constructor(props) {
        super(props);

        this.state = {travelTimeText:'na',
                      travelTimeValue: 0,
                      color:'black',
                      // warning:false,
                      // danger:false,
                      // success:false,
                      region:{
                        latitude:0,
                        longitude:0,
                        latitudeDelta:LATITUDE_DELTA,
                        longitudeDelta:LONGITUDE_DELTA
                      }};

        this.handleOnPress = this.handleOnPress.bind(this);
    }
    
    componentDidMount(){
      const {index} = this.props;
      const events = this.props.events.events;
      const {latitude, longitude, address, datetime} = events[index];
      const region = { latitude: latitude, longitude: longitude, latitudeDelta: LATITUDE_DELTA, longitudeDelta: LONGITUDE_DELTA };
      this.setState({region:region,
                      address: address});

      const origin = this.props.deviceLocation.lat + ',' + this.props.deviceLocation.long;
      const dest = latitude + ',' + longitude;
      travelTimeEst(origin, dest)
      .then(res=>{
        this.setState({
          travelTimeText: res.rows[0].elements[0].duration.text,
          travelTimeValue: res.rows[0].elements[0].duration.value});
          
          currentTime = Math.round((new Date().getTime())/1000)
          travelTime = res.rows[0].elements[0].duration.value;
          eventTime = this.props.events.events[this.props.index].datetime

          if (currentTime + travelTime > eventTime)
            this.setState({
              // danger:true,
              color: '#ff6347'
            });
          else if (currentTime + travelTime > eventTime - 1800 && currentTime + travelTime < eventTime)
            this.setState({
              // warning:true,
              color: '#ffd700'
            });
          else
            this.setState({
              // success: true,
              color:'#5f9ea0'
            });
      })
      .catch(err=>{
        console.log('Error setting travel time: ' + err);
        this.setState( {travelTimeText: 'na'});
      });
      
    }

    render() {

    return (
      
        <Card> 
            <CardItem button bordered={true} onPress={this.handleOnPress}>
              <Left>
                <Body>
                  <Text>{this.props.eventTitle}</Text>
              <Text note style={{color:this.state.color}}>{timeConverter(this.props.events.events[this.props.index].datetime, 1)}</Text>
                </Body>
              </Left>
              <Right>
              <Body>
              <Text>Estimated Travel Time</Text>
              <Text note>{this.state.travelTimeText}</Text>
                </Body>
              </Right>
            </CardItem>
            <CardItem bordered={true}>
              <MapView
                liteMode={true}
                provider={null}
                style={styles.map}
                scrollEnabled={false}
                zoomEnabled={false}
                pitchEnabled={false}
                rotateEnabled={false}
                initialRegion={this.state.region}
              >
                <Marker
                  title="This is a title"
                  coordinate={this.state.region}
                />
              </MapView>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Icon active name="md-pricetag" />
                  <Text>NT$ {this.props.deposit}</Text>
                </Button>
              </Left>
              <Button transparent onPress={this.handleOnPress}>
                <Icon active name="ios-arrow-forward-outline" />
              </Button>
            </CardItem>
          </Card>
        
    );
  }

  handleOnPress(){
    this.props.dispatch(NavigationActions.navigate({ routeName: 'Detail' , 
                                                     params: {index:this.props.index,
                                                              eventTitle: this.props.eventTitle,
                                                              datetime: this.props.events.events[this.props.index].datetime}
                                                    }
                                                  ));
  }
}

const Loading = () => (
  <View style={styles.container}>
    <Text>Loading...</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    width: 375,
    height: 200,
  },
});

export default connect((state) => {
  return {
      ...state.eventReducer
  };
})(EventItem);