import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Container, Header, Title, Button, Icon, Tabs, Tab, Right, Left, Body } from 'native-base';
import QRCode from 'react-native-qrcode';

import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { NavigationActions } from 'react-navigation';

export class SignInPage extends Component {

    constructor(props){
        super(props);

        this.state = {
            currentTimestamp: 0,
        }
    }

    render() {
        const {currentTimestamp} = this.state;
        const {userId, userName, userCoins, userPhoneNumber} = this.props;

        const qrData = JSON.stringify({
            "userId": userId,
            "userName": userName,
            "currentTimestamp": currentTimestamp
        });
        return (
			<Container>
                <Button block primary onPress={this.handleButtonTrigger}>
                    <Text>Save</Text>
                </Button>
                <Text>{currentTimestamp}</Text>
                <QRCode
                  value={qrData}
                  size={200}
                  bgColor='purple'
                  fgColor='white'/>
			</Container>
        );
    }

    handleButtonTrigger = () =>{
        const currentTimestamp = Math.floor(Date.now() / 1000);
        this.setState({currentTimestamp:currentTimestamp});
    }
}

export default connect((state, ownProps) => ({
    ...state.nav,
    ...state.userInfo,
}))(SignInPage);
