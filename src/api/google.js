// This is the api that use for travel time estimation using Google Distance Matrix API

const baseUrl = "https://maps.googleapis.com/maps/api/distancematrix/"

export function travelTimeEst(origins, dests, mode='driving', format='json'){
// Params
// origin : a string composed by "latitude,longititude". For example, 41.43206,-81.38992
// dest: same as origin
// mode: transporation, defualt is driving
// format: the response format, json is default

// // **********for debug*********/
//     origins = '25.039550,121.560265';
//     dests = '25.021918,121.535285';
// //*********** *****************/
    // let key = 'AIzaSyDmoKppGKoP5Vzt8dTz0tXyjTEYZbHyDhs';
    let key = 'AIzaSyCwzVrhvP7H8io8S3D2jJC2h7xBOJLkg0I';
    let url = `${baseUrl}${format}?units=imperial&origins=${origins}&destinations=${dests}&mode=${mode}&key=${key}`;
    // let url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=25.039550,121.560265&destinations=25.021918,121.535285&mode=driving&key=AIzaSyDmoKppGKoP5Vzt8dTz0tXyjTEYZbHyDhs'
    console.log(`Getting travel time info from ${url}`);
    return fetch(url,{
        method:'GET'
    }).then(res=>{
        if(res.ok){
            return res.json();
        }
        else
            throw new Error('Error getting request from google distance api');
    });

}

