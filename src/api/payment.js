import { AsyncStorage } from 'react-native';

/*
    local storage
*/
export function setPaymentList(paymentList){
    return AsyncStorage.setItem('payment_list', JSON.stringify(paymentList));

}


export function getPaymentList(){
    return AsyncStorage.getItem('payment_list');
}
