import {AsyncStorage} from 'react-native';
import {LoginButton, LoginManager, AccessToken} from 'react-native-fbsdk';
// gogole api key AIzaSyCH_RiXhxchoQA8Lr0UHJFU7BOnsjWhGis


const fbBaseUrl = 'https://graph.facebook.com/v3.0';


export function fbLoggedOut(){
  LoginManager.logOut((data)=>{
    this.props.onLogout();
  });
}

export function fbLoginManager(request_permissions=["public_profile", "user_friends"]) {
    LoginManager.logInWithReadPermissions(request_permissions).then(
      (result) => {
        if (result.isCancelled) {
          alert('Login was cancelled');
        } else {
          alert('Login was successful with permissions: '
            + result.grantedPermissions.toString());
          return AccessToken.getCurrentAccessToken().then((data) => {
            // AsyncStorage.setItem('@token', data.accessToken.toString());
            return data.accessToken.toString();          
          });
        }
      },
      (error)=> {
        alert('Login failed with error: ' + error);
      }
    );
  }
//the caller has to use promise statement
export function isLoggedIn(){
    return AccessToken.getCurrentAccessToken().then((data) => { 
        if(data!==null)
            return true;
        else
            return false;
    }).catch((error)=>{
        return false;
    });
}

export function getToken() {
    return AccessToken.getCurrentAccessToken().then((data) => {
        let url = `${fbBaseUrl}/me?access_token=${data.accessToken.toString()}`
        console.log(`check token: ${url}`);
        
        //if don't want to check if the token is valid, comment this return statement and uncomment the second return
        return fetch(url,{
          headers: {
              'Accept': 'application/json'
          }
        }).then((res)=>{
          if(res.ok){
            return data.accessToken.toString();
          } else {
            return fbLoginManager();
          }
        }).catch(error=>{
          throw new Error(`Get token error: ${error}`);
        });

        //do not validate the token 
        //return data.accessToken.toString();
    }).catch(error=>{
      return fbLoginManager();
    });
  }

export function getFriendList() {
  // AsyncStorage.getItem('@token').then(async (value)=>{console.log(value)});

  
  return getToken().then((token)=>{

    let url = `${fbBaseUrl}/me?fields=friends&access_token=${token}`;
    console.log(`Making GET request to: ${url}`);
    
    return fetch(url,{
        headers: {
            'Accept': 'application/json'
        }
    }).then((res)=>{
      if (res.status !== 200)
        throw new Error(`Unexpected response code: ${res.status}`);
      return res.json();  
    }).then((josn)=>{
      return josn.friends.data;
    }).catch(() => {
      console.log('ERROR GETTING DATA FROM FACEBOOK')
    });
  });

  
}


export function getUserInfo(query = ["id", "name"]){
    
    return getToken().then((token)=>{
        let queryInfo = query.toString();
        let url  = `${fbBaseUrl}/me?fields=${queryInfo}&access_token=${token}`;
        return fetch(url,{
            headers: {
                'Accept': 'application/json'
            }
        }).then((res)=>{
          if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`);
          return res.json();  
        }).then((josn)=>{
          return josn;
        }).catch(() => {
          console.log('ERROR GETTING DATA FROM FACEBOOK')
        });
    });

}

//pictType = large, normal, small, square, full, size you want
export function getUserPict(id, pictType="square"){
    
    if (pictType==="large" || pictType==="normal" || pictType==="small" || pictType==="square") {
      console.log(`GET user pict: ${fbBaseUrl}/${id}/picture?type=${pictType}`);
      return `${fbBaseUrl}/${id}/picture?type=${pictType}`;
    } else if (pictType==="full") {
      console.log(`GET user pict: ${fbBaseUrl}/${id}/picture?width=9999`);
      return `${fbBaseUrl}/${id}/picture?width=9999`;
    } else if (Number.isInteger(pictType)) {
      if(Number(pictType)>0){
        console.log(`GET user pict: ${fbBaseUrl}/${id}/picture?width=${pictType}`);
        return `${fbBaseUrl}/${id}/picture?width=${pictType}`;
      }
    }
}