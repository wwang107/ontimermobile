import { PushNotificationIOS, DeviceEventEmitter} from 'react-native';
import PushNotification from 'react-native-push-notification';
import PushNotificationAndroid from 'react-native-push-notification';
import {listEvents, getTravelTime, _getLocation, shareMoney} from './events';
import { travelTimeEst } from './google';
import {timeConverter} from "./utilities";
const baseUrl = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com";

export function setFireTime(userId, eventId, eventTime, alarmTime, userAuthority, eventInfo={}) {
	// let fireTime = eventTime - 24hr - userPrepareTime;
	// eventTime: unix time(sec)
	// userPrepareTime: minute

	//let fireTime = eventTime - userPrepareTime*60;				// unix(sec)
	let fireTime = eventTime - 86400;								// unix(sec)
	let timeUnix = new Date(fireTime*1000);							// unix(millisec)
	console.log('set notification: ', timeUnix, userId, eventId, eventTime, alarmTime, userAuthority);
	notifyOneDayBefore(timeUnix, userId, eventId, eventTime, alarmTime, userAuthority, eventInfo);
}

export function getEventTime(userId, eventId, eventTime, alarmTime, userAuthority, eventInfo) {
	let origin = "";
	let dest = "";
	console.log("userId: ", userId);
	console.log("eventId: ", eventId);
	console.log("eventTime: ", eventTime);
	console.log("alarmTime: ", alarmTime);
	console.log("userAuthority: ", userAuthority);
	listEvents(userId).then(res => {							// userid
		console.log("res: ", res);
		for (let i = 0; i < res.events.length; i++) {
			if (res.events[i].eventid == eventId) {
				dest = res.events[i].latitude + "," + res.events[i].longitude;
			}
		}
		console.log("dest: ", dest);
		return _getLocation({enableHighAccuracy: true, timeout: 30000});
	}).then(deviceInfo => {
		console.log("deviceInfo: ", deviceInfo);
		deviceLat = deviceInfo.coords.latitude;
		deviceLong = deviceInfo.coords.longitude;
		origin = deviceLat + ',' + deviceLong;
		console.log("origin: ", origin);
		return getTravelTime(origin, dest);							
	}).then(eta => {
		console.log("eta: ", eta);
		if (eta)
			var etaSec = eta.rows[0].elements[0].duration.value;
		else
			var etaSec = 60*60;
		//let etaSec = eta.
		console.log("etaSec: ", etaSec);
		// debugger
		let notifyTime = Number(eventTime) - etaSec - alarmTime*60;
		let overTime = Number(eventTime) + 1*60;
		let timeUnix = new Date(notifyTime*1000);
		let overTimeUnix = new Date(overTime*1000);
		console.log(`push notification startTime: ${notifyTime}) and overTime: ${overTime} ; eventTime:${eventTime}`);
		console.log(`push notification startTime: ${timeUnix}) and overTime: ${overTimeUnix}`);
		notifyEventStart(timeUnix, eventInfo);
		notifyEventOver(overTimeUnix, userAuthority, eventInfo);
	}).catch(error => {
		let notifyTime = Number(eventTime) - 60*60 - alarmTime*60;
		let overTime = Number(eventTime) + 1*60;
		let timeUnix = new Date(notifyTime*1000);
		let overTimeUnix = new Date(overTime*1000);
		console.log(`push notification startTime: ${notifyTime}) and overTime: ${overTime} ; eventTime:${eventTime}`);
		console.log(`push notification startTime: ${timeUnix}) and overTime: ${overTimeUnix}`);
		notifyEventStart(timeUnix, eventInfo);
		notifyEventOver(overTimeUnix, userAuthority, eventInfo);
		console.log(error);
	});
};

export function NotificationConfigure() {
    PushNotification.configure({
        onRegister: function(token) {
            //process token
            console.log('TOKEN:', token);
    	},
        
        onNotification: function(notification) {
       		// process the notification
            // required on iOS only
            console.log( 'NOTIFICATION:', notification);
       		notification.finish(PushNotificationIOS.FetchResult.NoData);
     	},

        permissions: {
       		alert: true,
       		badge: true,
       		sound: true
     	},

    	popInitialNotification: true,
    	requestPermissions: true,
	});
};

export function notifyOneDayBefore(timeUnix, userId, eventId, eventTime, alarmTime, userAuthority, eventInfo) {
	PushNotification.localNotificationSchedule({
		title: eventInfo.eventName,
		largeIcon: "ic_launcher",
		smallIcon: "ic_notification",
		bigText: "Date tomorrow!",
		subText: `Location: ${eventInfo.activityAddress}`,
		message: `${timeConverter(eventInfo.eventDateTime, 2)}`, // (required)
		color: "green",
		vibrate: true,
		vibration: 500,
    	playSound: true, // (optional) default: true
		soundName: 'ladadee.mp3',
		ongoing: true,
		actions: '["Confirm"]',
		date: timeUnix,
		userId: userId,
		eventId: eventId,
		eventTime: eventTime,
		alarmTime: alarmTime,
		authority: userAuthority,
		eventInfo: eventInfo,
	});
};

export function notifyEventStart(timeUnixeventInfo, eventInfo) {
	PushNotification.localNotificationSchedule({
		title: `${eventInfo.eventName} about to start`,
		largeIcon: "ic_launcher",
		smallIcon: "ic_notification",
		bigText: "Time to go!",
		subText: `Location: ${eventInfo.activityAddress}`,
		message: `${timeConverter(eventInfo.eventDateTime, 2)}`, // (required)
		color: "green",
		vibrate: true,
		vibration: 500,
    	playSound: true, // (optional) default: true
		soundName: 'ladadee.mp3',
		date: timeUnixeventInfo,
		eventInfo: eventInfo,
	});
}

export function notifyEventOver(overTimeUnix, userAuthority, eventInfo) {
	PushNotification.localNotificationSchedule({
		title: `Event end`,
		largeIcon: "ic_launcher",
		smallIcon: "ic_notification",
		bigText: `${eventInfo.eventName} is over`,
		subText: `${timeConverter(eventInfo.eventDateTime, 2)}`,
		message: ` Please check your deposit`, // (required)
		color: "green",
		vibrate: true,
		vibration: 500,
    	playSound: true, // (optional) default: true
		soundName: 'ladadee.mp3',
		ongoing: true,
		actions:'["Verify"]',
		date: overTimeUnix,
		authority: userAuthority,
		eventInfo: eventInfo,
	});
}

(function() {
	// Register all the valid actions for notifications here and add the action handler for each action
	PushNotificationAndroid.registerNotificationActions(['Confirm', 'Verify']);
	DeviceEventEmitter.addListener('notificationActionReceived', function(action){
	  	console.log ('Notification action received: ' + action);
		const info = JSON.parse(action.dataJSON);
		console.log("info:", info);
	
	  	if (info.action == 'Confirm') {
			getEventTime(info.userId, info.eventId, info.eventTime, info.alarmTime, info.authority, info.eventInfo);
		} else if (info.action == 'Verify') {
			//console.log("haha");
			if (info.authority == "hoster") {
				console.log("start to share money");
				// debugger
				// let msg = `${info.eventInfo.eventName} start to share money`
				alert("start to share money");
				shareMoney(info.eventInfo.eventId);
			}
		}

	  	// Add all the required actions handlers
	});
})();