import { AsyncStorage } from 'react-native';

const userBaseUrl = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com";

/*
    local storage
*/
// export function setOwnerId(ownerId){
//     return AsyncStorage.setItem('owner_id', ownerId);
//     try {
//         await AsyncStorage.setItem('owner_id', ownerId);
//     } catch (error) {
//         console.log("Error happened at userinfos api/setOwnerId");
//         console.error(error);
//     }
// }
//
//
// export function getOwnerId(){
//     try {
//         await AsyncStorage.getItem('owner_id');
//     } catch (error) {
//         console.log("Error happened at userinfos api/getOwnerId");
//         console.error(error);
//     }
// }

/*
    local storage
*/
export function storeUserInfoLocal(userInfo){
    return AsyncStorage.setItem('user_information', JSON.stringify(userInfo));
}

export function getUserInfoLocal(userInfo){
    return AsyncStorage.getItem('user_information');
}

/*
    get info from server
*/

export function getUserInfo(userId){
    let url = `${userBaseUrl}/api/userinfo?userid=${userId.toString()}`

    console.log(`Making GET request to to ${url}`);

    return fetch(url,{
        method:'GET'
    }).then(res=>{
        console.log(res);
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)

        return res.json();
    }).catch(err=>{
        console.log(err);
    })
}

// coins should not be modified by user, this is for debugging purpose
export function editUserInfo(userInfo){
  let url = `${userBaseUrl}/api/modifyuser`

  console.log(`Making POST request to to ${url}`);
  console.log(userInfo);
  test = JSON.stringify({
      "userid" : userInfo.userId,
      "username" : userInfo.userName,
      "usercoins" : userInfo.userCoins,
      "userphonenumber" : userInfo.userPhoneNumber
  });
  console.log(test);
  return fetch(url,{
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          "userid" : userInfo.userId,
          "username" : userInfo.userName,
          "usercoins" : userInfo.userCoins,
          "userphonenumber" : userInfo.userPhoneNumber
      })
  }).then(res=>{
      if (res.status !== 200)
          throw new Error(`Unexpected response code: ${res.status}`)
      return res.json();
  });
}

export function uploadUserPrepareTime(userPrepareTime, userId){

    let url = `${userBaseUrl}/api/modifypreparetime`;
    console.log(`Making GET request to to ${url}`);
    console.log(JSON.stringify({
          "userid" : userId,
          "preparetime": userPrepareTime,
          
      }));
    return fetch(url,{
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          "userid" : userId,
          "preparetime": userPrepareTime,
          
      })
  }).then(res=>{
      if (res.status !== 200)
          throw new Error(`Unexpected response code: ${res.status}`)
      return res.json();
  });
}

// for debugging purpose
export function createUser(userInfo){
  let url = `${userBaseUrl}/api/adduser`

  console.log(`Making POST request to to ${url}`);
  return fetch(url,{
      method: 'POST',
      headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          "userid" : userInfo.userId,
          "username" : userInfo.userName,
          "userphonenumber" : userInfo.userPhoneNumber
      })
  }).then(res=>{
      if (res.status !== 200)
          throw new Error(`Unexpected response code: ${res.status}`)

      return res.json();
  }).catch(err=>{
    console.log(err);
  });
}

export function calculatePrepareTime(eventId, userId){
    let url = `${userBaseUrl}/api/memberinfo?eventid=${eventId}&userid=${userId}`;

    console.log(`Making GET request to to ${url}`);
    return fetch(url,{
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)

        return res.json();
    }).catch(err=>{
      console.log(err);
    });
}
