const googleApiKey = 'AIzaSyCwzVrhvP7H8io8S3D2jJC2h7xBOJLkg0I';
const baseUrl = `https://maps.googleapis.com/maps/api/geocode/json?key=${googleApiKey}&sensor=true`;

//other api please refer to https://developers.google.com/maps/documentation/
//sample query: https://maps.googleapis.com/maps/api/geocode/json?address=Taipei&key= AIzaSyCwzVrhvP7H8io8S3D2jJC2h7xBOJLkg0I

export function getGeoByAddress(address) {
    let query = `${baseUrl}&address=${address}`
    return fetch(query,{
        headers: {
            'Accept': 'application/json'
        }
    }).then((res)=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`);
        return res.json();  
    }).then((json)=>{
        return ({
            ...json.results[0].geometry.location,           
        });
    }).catch(() => {
        console.log('ERROR GETTING DATA FROM FACEBOOK')
    });
}

