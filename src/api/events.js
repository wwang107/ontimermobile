import { travelTimeEst} from './google'

// Develop server URL
const baseUrl = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com/api";


export function listEvents(userId = 1708356575913015){
    //debugger
    let userUrl = `${baseUrl}/events?userid=${userId}`;
    let eventUrl = `${baseUrl}/eventinfo?eventid=`;
    
    
    let Data = {"user":{},"events":{}};


    return fetch(userUrl,{
        method:'GET'
    // this will return atmost 10 events that the userId involved 
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)
        return res.json();
    }).then(array => {
        Data["user"] = array.reverse();
        let request = [];
        eventIds = array.map(item=>item.eventid);
        for (let i = 0; i < eventIds.length; i++) {
            url = `${eventUrl}${eventIds[i]}`;
            request.push(
                fetch(url, { method: 'GET' }).then(res => {
                    return res.json()
                })
            );
        }
        

        // request.push(_getLocation({ enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }))
        
        return Promise.all(request)
                .then(request => {
                    events = []
                    for (let i=0; i<request.length; i++){
                        events.push(request[i][0]);
                    }
                    // console.log(request);
                    Data["events"] = events;
                    // Data["deviceLocation"] = request[request.length - 1];
                    console.log('In event api')
                    console.log(Data);
                    return Data;
                })
                .catch(err=>{
                    console.log(request);
                    throw new Error(`Unexpected response code: ${err}`);
                });
    })    
}


export function getTravelTime(origins, dests){
    return travelTimeEst(origins, dests)
           .then((estimation)=>{
            return estimation;
           }); 
}

export function _getLocation(options) {
    return new Promise((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
}


export function createEvent(value){
    let url = `${baseUrl}/createvents`;
    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "eventname":value.eventName,
            "datetime":value.eventDateTime,
            "mindeposit":value.minDeposit,
            "maxdeposit":value.maxDeposit,
            "address": value.activityAddress,
            "about":value.about,
            "latitude":value.latitude,
            "longitude":value.longitude,
            "hoster": value.hoster,
            "hostername":value.hostName
        })
    }).then(function(res) {
        if (res.status !== 200){
            throw new Error(`Unexpected response code: ${res.status}`);
        }
        else{
            return res.json();
        }
    }).then((json)=>{
        return json;
    }).catch((error)=>{
        throw new Error(`Error create events in events.js: ${error}`);
    });
}

export function confirmOrJoin (userObj) {
    return confirmEvent(userObj).then((confirmObj)=>{
        if(confirmObj){
            return confirmObj;
        }else{
            return addMember(userObj);
        }
    }).catch((error)=>{
        throw new Error(`error confirm or join : ${error}`);
    });
}

export function confirmEvent(userObj){
    let url = `${baseUrl}/confirmevents?eventid=${userObj.eventId}&userid=${userObj.userId}&deposit=${userObj.deposit}`;
    return fetch(url,{
        method:'GET'
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)
        return res.json();
    }).then(array => {
        return array[0];
    });
}

export function addMember(userObj){
    let url = `${baseUrl}/addmembers`;
    return fetch(url, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "eventid": userObj.eventId,
            "eventname": userObj.eventName,
            "deposit": userObj.deposit,
            "username": userObj.userName, //require
            "userid": userObj.userId,
            "hoster": userObj.hoster,
            "hostername": userObj.hostName,
            "datetime": userObj.datetime,
            "alarmtime":userObj.alarmTime,
        })
    }).then((res)=>{
        if (res.status !== 200){
            throw new Error(`Unexpected response code: ${res.status}`);
        }
        else{
            return res.json();
        }
    }).then((json)=>{
        return json;
    }).catch((error)=>{
        throw new Error(`Error add members in events.js: ${error}`);
    });
}

export function invitemembers(userObjs){
    let url = `${baseUrl}/invitemembers`;
    let requests = [];
    for(let i = 0 ; i<userObjs.length ; i ++){
        let r = fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "userid": userObjs[i].userId,
                "username": userObjs[i].userName, //require
                "eventid": userObjs[i].eventId,
                "eventname": userObjs[i].eventName,
                "deposit": userObjs[i].deposit,
                "hoster": userObjs.hoster,
                "hostername": userObjs[i].hostName,
                "datetime": userObjs[i].datetime,
                "alarmtime":userObjs[i].alarmTime,
            })
        });
        requests.push(r);
    }
    return Promise.all(requests).then((res)=>{
        // console.log(res);
        return res;
    }).catch((error)=>{
        throw new Error(`Error add members in events.js: ${error}`);
    });
}

export function addMembers(userObjs){
    let url = `${baseUrl}/addmembers`;
    let request = [];
    for(let i = 0 ; i<userObjs.length ; i ++){
        let r = fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "eventid": userObjs[i].eventId,
                "eventname": userObjs[i].eventName,
                "deposit": userObjs[i].deposit,
                "username": userObjs[i].userName, //require
                "userid": userObjs[i].userId,
                "hostername": userObjs[i].hostName,
                // "confirm": userObjs[i].confirm,
                "datetime": userObjs[i].datetime,
                "alarmtime":userObjs[i].alarmTime,
            })
        });
        request.push(r);
    } 
    return Promise.all(request).then((res)=>{
        return res;
    }).catch((error)=>{
        throw new Error(`Error add members in events.js: ${error}`);
    });
}

export function getEventInfoById(eventId){
    let url = `${baseUrl}/eventinfo?eventid=${eventId}`;
    return fetch(url,{
        method:'GET'
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)
        return res.json();
    }).then(array => {
        return array[0];
    });
}

export function signInEvent(eventId, datetime, userId = 1708356575913015 ){
    let url = `${baseUrl}/arrive`

    let currentDate  = Math.round(new Date().getTime()/1000);
    let late = false;
    if (currentDate > datetime)
        late = true;
    
    let requestBody = {
        "eventid": eventId,
        "userid": userId,
        "arrivetime": currentDate,
        "late": late
    }

    console.log('sending sign in event request to: ', url);
    return fetch(url,{
        method:'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(requestBody)
    });
}

export function shareMoney(eventId) {
    let url = `${baseUrl}/sharemoney?eventid=${eventId}`;
    console.log(`POST: ${url}`);
    return fetch(url,{
        method:'GET'
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)
        return res.json();
    }).then(array => {
        return array[0];
    });
}