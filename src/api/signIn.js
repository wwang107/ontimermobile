import { AsyncStorage } from 'react-native';

const baseUrl = "http://ontimer-server-dev.us-west-2.elasticbeanstalk.com";

/*
    post sign in info
*/

export function postSignin(userInfo){
    let url = `${baseUrl}/api/arrive`

    console.log(`Making POST request to to ${url}`);

    return fetch(url,{
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "eventid" : userInfo.eventId,
            "userid" : userInfo.userId,
            "arrivetime" : userInfo.currentTimestamp,
            "late" : userInfo.isLate,
        })
    }).then(res=>{
        if (res.status !== 200)
            throw new Error(`Unexpected response code: ${res.status}`)

        // return data that should be used to refresh the ui
        return res.json();
    });
}
