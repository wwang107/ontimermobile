function addLeadingZero(e) {
    return (e < 10 ? ('0' + e) : e);  // avoid something like '5:20:4' or '2018/6/3'
}

export function timeConverter(UNIX_timestamp, format) {
    var defaultFormat = new Date(UNIX_timestamp * 1000);
    var year = addLeadingZero(defaultFormat.getFullYear());
    var month = addLeadingZero(defaultFormat.getMonth() + 1);
    var date = addLeadingZero(defaultFormat.getDate());
    var hour = addLeadingZero(defaultFormat.getHours());
    var min = addLeadingZero(defaultFormat.getMinutes());
    var sec = addLeadingZero(defaultFormat.getSeconds());
    var time;

    switch(format) {
        case 0:
            time = year + '/' + month + '/' + date;  // ex: 2018/06/25
            break;
        case 1:
            time = year + '/' + month + '/' + date + '  ' + hour + ':' + min + ':' + sec;  // ex: 2018/06/25  18:05:35
            break;
        case 2:
            time = year + '/' + month + '/' + date + '  ' + hour + ':' + min;  // ex: 2018/06/25  18:05
            break;
        default:
            time = defaultFormat;  // ex: Thu Jun 21 2018 23:22:29 GMT+0800 (台北標準時間)
    }
    return time;
}